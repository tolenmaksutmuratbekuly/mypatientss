package com.vienna.mypatients;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.vienna.mypatients.config.DebugConfig;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.data.cache.preferences.PreferencesProvider;
import com.vienna.mypatients.utils.LocaleUtils;
import com.vienna.mypatients.utils.stetho.StethoCustomConfigBuilder;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class MyPatientsApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        setupFabric();
        setupTimber();
        if (DebugConfig.DEV_BUILD) {
            setupStetho();
        }

        Preferences preferences = new PreferencesProvider(this);
        LocaleUtils langUtil = new LocaleUtils(preferences, this);
        langUtil.fixLocale(langUtil.getLocale());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    private void setupTimber() {
        if (DebugConfig.DEV_BUILD) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new Timber.DebugTree());
//            Timber.plant(new CrashlyticsTree());
        }
    }

    private void setupStetho() {
        Stetho.Initializer initializer = new StethoCustomConfigBuilder(this)
                .viewHierarchyInspectorEnabled(false)
                .build();
        Stetho.initialize(initializer);
    }

    private void setupFabric() {
        Fabric.with(this, new Crashlytics());
    }
}