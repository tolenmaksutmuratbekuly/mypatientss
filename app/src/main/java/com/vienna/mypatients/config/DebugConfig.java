package com.vienna.mypatients.config;

import com.vienna.mypatients.BuildConfig;

public class DebugConfig {
    public static final boolean DEV_BUILD = BuildConfig.DEBUG;
}
