package com.vienna.mypatients.config;

import com.vienna.mypatients.BuildConfig;

public class MyPatientsConfig {
    public static final String SCHEME = "https://";
    public static final String DOMAIN = BuildConfig.URL_BASE;
    public static final String API_BASE_URL = SCHEME + DOMAIN + "v2/";
}
