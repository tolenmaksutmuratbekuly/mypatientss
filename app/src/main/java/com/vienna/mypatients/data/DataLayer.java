package com.vienna.mypatients.data;

import android.content.Context;

import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.data.cache.preferences.PreferencesProvider;
import com.vienna.mypatients.data.network.RetrofitServiceGenerator;
import com.vienna.mypatients.data.network.api.API;

public class DataLayer {
    private static volatile DataLayer instance;

    public static DataLayer getInstance(Context context) {
        if (instance == null) {
            synchronized (DataLayer.class) {
                if (instance == null) instance = new DataLayer(context.getApplicationContext());
            }
        }
        return instance;
    }

    private final PreferencesProvider preferencesProvider;
    private final API api;

    private DataLayer(Context context) {
        preferencesProvider = new PreferencesProvider(context);
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferencesProvider);
        api = retrofitServiceGenerator.createService(API.class);
    }

    public Preferences getPreferences() {
        return preferencesProvider;
    }

    public void wipeOut() {
        preferencesProvider.clearAllPreferences();
    }

    public API getApi() {
        return api;
    }
}
