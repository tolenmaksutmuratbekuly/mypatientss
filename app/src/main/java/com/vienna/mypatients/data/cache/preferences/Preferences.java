package com.vienna.mypatients.data.cache.preferences;

public interface Preferences {
    void clearAllPreferences();

    /****** Language *******/
    void setLanguage(String language);

    String getLanguage();

    /****** Agreement *******/
    void setShownAgreement(boolean shown);

    boolean hasShownAgreement();

    /****** Authorization *******/
    boolean isAuthenticated();

    void setAccessToken(String accessToken);

    String getAccessToken();

    void setClient(String client);

    String getClient();

    void setUid(String uid);

    String getUid();

    /****** User *******/

    void setProfileImage(String profileImage);

    String getProfileImage();

    void setFullName(String fullName);

    String getFullName();

    void setUserIIN(String iin);

    String getUserIIN();

}
