package com.vienna.mypatients.data.cache.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static android.text.TextUtils.isEmpty;
import static com.vienna.mypatients.utils.Constants.AGREEMENT;
import static com.vienna.mypatients.utils.Constants.CLIENT;
import static com.vienna.mypatients.utils.Constants.KEY_FULL_NAME;
import static com.vienna.mypatients.utils.Constants.KEY_IMG;
import static com.vienna.mypatients.utils.Constants.KEY_USER_IIN;
import static com.vienna.mypatients.utils.Constants.LOCALE_LANG;
import static com.vienna.mypatients.utils.Constants.UID;
import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;

public class PreferencesProvider implements Preferences {
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private final SharedPreferences sharedPreferences;

    public PreferencesProvider(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Preferences implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void clearAllPreferences() {
        synchronized (sharedPreferences) {
            // Don't clear the HAS_SHOWN_AGREEMENT & LOCALE_LANG key,
            // because we don't want to show agreement & language again when user logs out.
            boolean shownAgreement = sharedPreferences.getBoolean(AGREEMENT, false);
            String language = sharedPreferences.getString(LOCALE_LANG, EMPTY_STRING);
            sharedPreferences.edit().clear()
                    .putBoolean(AGREEMENT, shownAgreement)
                    .putString(LOCALE_LANG, language)
                    .apply();
        }
    }

    @Override
    public void setLanguage(String language) {
        sharedPreferences.edit().putString(LOCALE_LANG, language).apply();
    }

    @Override
    public String getLanguage() {
        return sharedPreferences.getString(LOCALE_LANG, EMPTY_STRING);
    }

    @Override
    public void setShownAgreement(boolean shown) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(AGREEMENT, shown).apply();
        }
    }

    @Override
    public boolean hasShownAgreement() {
        return sharedPreferences.getBoolean(AGREEMENT, false);
    }

    @Override
    public boolean isAuthenticated() {
        return !isEmpty(sharedPreferences.getString(ACCESS_TOKEN, null));
    }

    @Override
    public void setAccessToken(String accessToken) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).apply();
        }
    }

    @Override
    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN, EMPTY_STRING);
    }

    @Override
    public void setClient(String client) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(CLIENT, client).apply();
        }
    }

    @Override
    public String getClient() {
        return sharedPreferences.getString(CLIENT, EMPTY_STRING);
    }

    @Override
    public void setUid(String uid) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(UID, uid).apply();
        }
    }

    @Override
    public String getUid() {
        return sharedPreferences.getString(UID, EMPTY_STRING);
    }

    @Override
    public void setProfileImage(String profileImage) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(KEY_IMG, profileImage).apply();
        }
    }

    @Override
    public String getProfileImage() {
        return sharedPreferences.getString(KEY_IMG, EMPTY_STRING);
    }

    @Override
    public void setFullName(String fullname) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(KEY_FULL_NAME, fullname).apply();
        }
    }

    @Override
    public String getFullName() {
        return sharedPreferences.getString(KEY_FULL_NAME, EMPTY_STRING);
    }

    @Override
    public void setUserIIN(String iin) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(KEY_USER_IIN, iin).apply();
        }
    }

    @Override
    public String getUserIIN() {
        return sharedPreferences.getString(KEY_USER_IIN, EMPTY_STRING);
    }
}
