package com.vienna.mypatients.data.models.areas;

import com.vienna.mypatients.data.models.profile.Statistic;

import java.util.Date;

public class Area {
    public String id;
    public String name;
    public Date created_at;
    public City city;
    public Statistic statistic;
}
