package com.vienna.mypatients.data.models.areas;

import com.vienna.mypatients.data.models.auth.Organization;
import com.vienna.mypatients.data.models.profile.Statistic;

public class AreaDetail {
    public AreaDetailLocal data;

    public class AreaDetailLocal extends Area {
        public Organization organization;
        public PrincipalDoctor principal_doctor;
        public Statistic area_statistic;
    }

    public class PrincipalDoctor {
        public String id;

        public String name;
    }
}
