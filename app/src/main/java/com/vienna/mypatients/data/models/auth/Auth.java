package com.vienna.mypatients.data.models.auth;

import com.vienna.mypatients.data.models.areas.Area;

import java.util.Date;

public class Auth {
    public String id;
    public String iin;
    public String full_name;
    public String email;
    public String qualification;
    public Date created_at;
    public Date updated_at;
    public String phone;
    public String specialization;
    public String role_name;
    public String thumb;
    public Integer status;
    public Boolean is_supervisor;
    public Area area;
    public Organization organization;
    public Role role;

    public class Role {
        public String id;
        public String child_id;
        public String title;
        public Date created_at;
        public Date updated_at;
        public String kind;
        public Boolean is_default;
    }
}

