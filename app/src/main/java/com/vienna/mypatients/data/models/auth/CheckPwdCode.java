package com.vienna.mypatients.data.models.auth;

import com.vienna.mypatients.utils.StringUtils;

public class CheckPwdCode {
    public Data data;

    private class Data {
        String token;
    }

    public String getToken() {
        if (data != null) {
            return StringUtils.length(data.token) > 0 ? data.token : StringUtils.EMPTY_STRING;
        } else {
            return StringUtils.EMPTY_STRING;
        }
    }

}
