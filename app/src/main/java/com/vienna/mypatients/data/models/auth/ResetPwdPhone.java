package com.vienna.mypatients.data.models.auth;

import com.vienna.mypatients.utils.StringUtils;

public class ResetPwdPhone {
    public Data data;

    private class Data {
        String phone;
    }

    public String getPhone() {
        if (data != null) {
            return StringUtils.length(data.phone) > 0 ? data.phone : StringUtils.EMPTY_STRING;
        } else {
            return StringUtils.EMPTY_STRING;
        }
    }
}
