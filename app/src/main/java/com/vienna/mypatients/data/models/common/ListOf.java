package com.vienna.mypatients.data.models.common;

import com.vienna.mypatients.data.models.patients.Meta;

import java.util.List;

public class ListOf<T>{

    public List<T> data;

    public Meta meta;
}
