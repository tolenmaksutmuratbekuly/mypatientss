package com.vienna.mypatients.data.models.extra;

import com.vienna.mypatients.utils.StringUtils;

public class Agreement {
    public Content content;

    private class Content {
        public String text;
    }

    public String getText() {
        if (content != null) {
            return StringUtils.length(content.text) > 0 ? content.text : StringUtils.EMPTY_STRING;
        } else {
            return StringUtils.EMPTY_STRING;
        }
    }
}