package com.vienna.mypatients.data.models.extra;

import java.util.Date;

import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;
import static com.vienna.mypatients.utils.StringUtils.isStringOk;

public class Versions {
    public String id;
    public String os;
    public String kind;
    public String version;
    public String text_ru;
    public String text_kk;
    public Date created_at;

    public String getVersion() {
        return isStringOk(version) ? version : EMPTY_STRING;
    }

    public String getTextRU() {
        return isStringOk(text_ru) ? text_ru : EMPTY_STRING;
    }

    public String getTextKZ() {
        return isStringOk(text_kk) ? text_kk : EMPTY_STRING;
    }
}
