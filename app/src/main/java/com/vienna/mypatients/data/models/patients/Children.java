package com.vienna.mypatients.data.models.patients;

public class Children {

    public String id;

    public String gender;

    public String weight;

    public String height;

    public String apgar_score;
}
