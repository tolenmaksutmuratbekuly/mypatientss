package com.vienna.mypatients.data.models.patients;


public class Meta {

    public int total_entries;

    public int count;

    public int current_page;

    public int total_pages;

    public int per_page;

    public int offset;
}
