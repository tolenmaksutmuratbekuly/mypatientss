package com.vienna.mypatients.data.models.patients;

import com.vienna.mypatients.data.models.areas.Area;

import java.util.Date;

public class Patient {

    public String id;

    public String iin;

    public String full_name;

    public Date created_at;

    public String image;

    public Date birth_date;

    public String phone;

    public String age;

    public Date rb_last_synced_at;

    public String education;

    public String benefits;

    public String social_status;

    public String martial_status;

    public String blood_group;

    public String blood_rh;

    public String nationality;

    public Area area;

    public Pregnancy pregnancy;

    public Doctor responsible_doctor;
}
