package com.vienna.mypatients.data.models.patients;


public class PatientShort {

    public String id;

    public String iin;

    public String full_name;

    public String image;

    public String status;

    public String organization;

    public String area;
}
