package com.vienna.mypatients.data.models.patients;

import java.util.Date;

public class Pregnancy {

    public String id;

    public String status;

    public Date start_date;

    public int week;

    public Date birth_date;

    public Date end_date;

    public String outcome;

    public String outcome_description;

    public String postnatal_days;
}
