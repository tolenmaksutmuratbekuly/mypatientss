package com.vienna.mypatients.data.models.profile;

import org.parceler.Parcel;

@Parcel
public class Statistic {
    public Complaints complaints;
    public Questions questions;
    public Patients patients;
    public Integer unadvised_complaints;
    public Integer unanswered_questions;

    public int getUnadvisedComplains() {
        return unadvised_complaints != null ? unadvised_complaints : 0;
    }

    public int getUnansweredQuestions() {
        return unanswered_questions != null ? unanswered_questions : 0;
    }

    //==========================================================================================================
    @Parcel
    public static class Complaints {
        public Integer all;
        public Integer advised;
        public Integer unadvised;
    }

    public int getAllComplaints() {
        return (complaints != null && complaints.all != null) ? complaints.all : 0;
    }

    public int getComplaintsUnadvised() {
        return (complaints != null && complaints.unadvised != null) ? complaints.unadvised : 0;
    }

    public int getComplaintsAdvised() {
        return (complaints != null && complaints.advised != null) ? complaints.advised : 0;
    }

    //==========================================================================================================
    @Parcel
    public static class Questions {
        public Integer all;
        public Integer answered;
        public Integer unansered;
        public Integer unanswered;
    }

    public int getAllQuestions() {
        return (questions != null && questions.all != null) ? questions.all : 0;
    }

    public int getUnanswered() {
        return (questions != null && questions.unanswered != null) ? questions.unanswered : 0;
    }

    public int getUnansered() {
        return (questions != null && questions.unansered != null) ? questions.unansered : 0;
    }

    public int getAnswered() {
        return (questions != null && questions.answered != null) ? questions.answered : 0;
    }

    //==========================================================================================================
    @Parcel
    public static class Patients {
        public Integer all;
        public Integer prenatal_fewer;
        public Integer postnatal_fewer;
        public Integer prenatal_over;
        public Integer postnatal_over;
        public Integer prenatal;
        public Integer postnatal;
        public Integer over;
        public Integer complete;
    }

    public int getAllPatientsSum() {
        return (patients != null && patients.all != null) ? patients.all : 0;
    }

    public int getPrenatalOver() {
        return (patients != null && patients.prenatal_over != null) ? patients.prenatal_over : 0;
    }

    public int getPostnatalOver() {
        return (patients != null && patients.postnatal_over != null) ? patients.postnatal_over : 0;
    }

    public int getPrenatal() {
        return (patients != null && patients.prenatal != null) ? patients.prenatal : 0;
    }

    public int getPostnatal() {
        return (patients != null && patients.postnatal != null) ? patients.postnatal : 0;
    }

    public int getPrenatalFewer() {
        return (patients != null && patients.prenatal_fewer != null) ? patients.prenatal_fewer : 0;
    }

    public int getPostnatalFewer() {
        return (patients != null && patients.postnatal_fewer != null) ? patients.postnatal_fewer : 0;
    }

    public int getPatientsOver() {
        return (patients != null && patients.over != null) ? patients.over : 0;
    }

    public int getPatientsComplete() {
        return (patients != null && patients.complete != null) ? patients.complete : 0;
    }
}