package com.vienna.mypatients.data.models.profile;

import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.auth.Organization;

import java.util.Date;
import java.util.List;

import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;

public class User {
    public Data data;

    public class Data {
        public String id;
        public String iin;
        public String full_name;
        public String email;
        public String qualification;
        public Date created_at;
        public Date updated_at;
        public String phone;
        public String specialization;
        public String thumb;
        public Organization organization;
        public Role role;
        public Statistic statistic;
        public List<Area> areas;

        public String getRole() {
            return (role != null && role.label != null) ? role.label : EMPTY_STRING;
        }
    }

    public class Role {
        public String id;
        public String title;
        public String label;
    }
}

