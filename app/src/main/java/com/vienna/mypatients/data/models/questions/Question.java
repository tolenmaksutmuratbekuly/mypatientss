package com.vienna.mypatients.data.models.questions;

import com.vienna.mypatients.data.models.patients.Doctor;
import com.vienna.mypatients.data.models.patients.PatientShort;

import java.util.Date;

public class Question {

    public String id;

    public String status;

    public String question_text;

    public Date created_at;

    public String answer_text;

    public Date answered_at;

    public String comments_count;

    public PatientShort patient;

    public Doctor doctor;
}
