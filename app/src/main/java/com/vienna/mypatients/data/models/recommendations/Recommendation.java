package com.vienna.mypatients.data.models.recommendations;

import java.util.Date;
import java.util.List;

public class Recommendation {

    public String id;

    public String image;

    public Date created_at;

    public Date updated_at;

    public int likes_count;

    public int readed_count;

    public List<String> locales;

    public String thumb;

    public Content content;

    public String category_id;

    public RecommendationCategory category;
}
