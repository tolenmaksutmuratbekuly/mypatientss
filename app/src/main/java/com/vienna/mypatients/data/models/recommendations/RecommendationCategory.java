package com.vienna.mypatients.data.models.recommendations;


import java.util.Date;
import java.util.List;

public class RecommendationCategory {

    public String id;

    public String kind;

    public List<String> locales;

    public Name name;

    public Date created_at;

    public Date updated_at;

    public String thumb;
}
