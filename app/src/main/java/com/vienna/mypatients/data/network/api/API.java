package com.vienna.mypatients.data.network.api;

import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.data.models.auth.Auth;
import com.vienna.mypatients.data.models.auth.CheckPwdCode;
import com.vienna.mypatients.data.models.auth.ResetPwdPhone;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.extra.Agreement;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.data.models.patients.PatientData;
import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.data.params.auth.AuthParams;
import com.vienna.mypatients.data.params.auth.ForgotPwdParams;
import com.vienna.mypatients.data.params.auth.NewPwdParams;
import com.vienna.mypatients.data.params.my_patients.FinishPostnatalParams;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface API {
    /****** Extra *******/
    @GET("agreements/my_pregnancy")
    Observable<Agreement> getAgreement();

    @GET("doctor/changelogs/android")
    Observable<ListOf<Versions>> getVersionsList(@Query("page[number]") int number,
                                                 @Query("page[size]") int count);

    /****** Authorization *******/
    @POST("doctor/sign_in")
    Observable<Auth> signIn(@Body AuthParams params);

    /****** Restore password *******/
    @GET("doctor/profile/reset_password_phone")
    Observable<ResetPwdPhone> resetPwdPhone(@Query("iin") String iin);

    @POST("doctor/profile/reset_password")
    Observable<ResponseBody> resetPwd(@Body ForgotPwdParams params);

    @POST("doctor/profile/check")
    Observable<CheckPwdCode> checkPwdCode(@Body ForgotPwdParams params);

    @POST("doctor/password")
    Observable<ResponseBody> newPassword(@Body NewPwdParams params);

    /****** User *******/
    @GET("doctor/my_profile")
    Observable<User> getProfile();

    @Multipart
    @POST("doctor/profile/set_photo")
    Observable<User> setPhoto(@Part MultipartBody.Part photo);

    /****** Patients ******/
    @GET("doctor/my_patients")
    Observable<ListOf<Patient>> getListOfPatients(@Query("page[number]") int number,
                                                  @Query("page[size]") int count,
                                                  @Query("full_name") String full_name,
                                                  @Query("iin") String iin,
                                                  @Query("prenatal_over") boolean prenatal_over,
                                                  @Query("postnatal_over") boolean postnatal_over,
                                                  @Query("transfer_over") boolean transfer_over
    );


    @GET("doctor/my_patients/{patient_id}/children")
    Observable<ListOf<Children>> getListOfChildren(@Path("patient_id") String patient_id);

    @GET("doctor/my_patients/{patient_id}")
    Observable<PatientData> getPatient(@Path("patient_id") String patient_id);

    @PUT("doctor/my_patients/{patient_id}/pregnancies/{pregnancies_id}")
    Observable<ResponseBody> finishPostnatal(@Path("patient_id") String patient_id,
                                             @Path("pregnancies_id") String pregnancies_id,
                                             @Body FinishPostnatalParams params);

    /****** Areas ******/
    @GET("doctor/areas")
    Observable<ListOf<Area>> getAreasList(@Query("page[number]") int number,
                                          @Query("page[size]") int count);

    @GET("doctor/areas/{id}")
    Observable<AreaDetail> getAreaDetail(@Path("id") String id);


    /****** Recommendations ******/
    @GET("doctor/recommendation_categories")
    Observable<ListOf<RecommendationCategory>> getRecommendationCategory(@Query("kind") String kind);

    @GET("doctor/recommendations")
    Observable<ListOf<Recommendation>> getRecommendations(@Query("category_id") String category_id);

    /****** TechSupport ******/

    /****** Questions ******/
    @GET("/api/v2/doctor/questions")
    Observable<ListOf<Question>> getQuestions();

}