package com.vienna.mypatients.data.network.error;

import com.vienna.mypatients.data.network.GsonProvider;
import com.vienna.mypatients.data.network.exceptions.APIException;
import com.vienna.mypatients.data.network.exceptions.ComingSoonException;
import com.vienna.mypatients.data.network.exceptions.ConnectionTimeOutException;
import com.vienna.mypatients.data.network.exceptions.NeedReAuthorizeException;
import com.vienna.mypatients.data.network.exceptions.UnknownException;
import com.vienna.mypatients.utils.StringUtils;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

public class RetrofitErrorHandler {
    private static final int UNAUTHORIZED = 401;
    private static final int COMING_SOON = 502;

    public static void handleException(Throwable e) throws
            APIException,
            UnknownException,
            ConnectionTimeOutException,
            NeedReAuthorizeException,
            ComingSoonException {

        if (e instanceof NeedReAuthorizeException) {
            throw new NeedReAuthorizeException();
        } else if (e instanceof ComingSoonException) {
            throw new ComingSoonException();
        } else if (e instanceof HttpException) {
            handleHttpException(e);
        } else if (e instanceof IOException) {
            throw new ConnectionTimeOutException();
        } else {
            throw new UnknownException();
        }
    }

    private static void handleHttpException(Throwable e)
            throws
            APIException,
            UnknownException,
            NeedReAuthorizeException,
            ComingSoonException {

        HttpException exception = (HttpException) e;
        if (exception.code() == UNAUTHORIZED) {
            throw new NeedReAuthorizeException();
        } else if (exception.code() == COMING_SOON) {
            throw new ComingSoonException();
        }
        Response response = exception.response();
        if (response != null) {
            APIError apiError;
            try {
                apiError = parseError(response);
            } catch (Exception e1) {
                e1.printStackTrace();
                throw new UnknownException();
            }

            if (apiError != null) {
                String errorMessage = StringUtils.replaceNull(apiError.getErrors());
                throw new APIException(errorMessage);
            }
        }
        throw new UnknownException();
        //handle JsonParseException, occured when html page returned instead of JSON
//        }
    }

    private static APIError parseError(Response<?> response) throws IOException {
        return GsonProvider.gson.fromJson(response.errorBody().string(), APIError.class);
    }
}