package com.vienna.mypatients.data.network.interceptors;

import com.vienna.mypatients.data.cache.preferences.Preferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.vienna.mypatients.utils.Constants.ACCEPT_LANGUAGE;
import static com.vienna.mypatients.utils.Constants.ACCESS_TOKEN;
import static com.vienna.mypatients.utils.Constants.CLIENT;
import static com.vienna.mypatients.utils.Constants.UID;

public class UserTokenInterceptor implements Interceptor {
    private Preferences preferences;

    public UserTokenInterceptor(Preferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request.Builder newRequestBuilder = originalRequest.newBuilder();
        if (preferences.isAuthenticated()) {
            newRequestBuilder.header(ACCESS_TOKEN, preferences.getAccessToken());
            newRequestBuilder.header(UID, preferences.getUid());
            newRequestBuilder.header(CLIENT, preferences.getClient());
        }

        newRequestBuilder.header(ACCEPT_LANGUAGE, preferences.getLanguage());
        newRequestBuilder.method(originalRequest.method(), originalRequest.body());
        Request newRequest = newRequestBuilder.build();
        return chain.proceed(newRequest);
    }
}