package com.vienna.mypatients.data.params.auth;

import android.support.annotation.StringRes;

import com.vienna.mypatients.R;
import com.vienna.mypatients.utils.Constants;
import com.vienna.mypatients.utils.StringUtils;

import static android.text.TextUtils.isEmpty;

public class AuthParams {
    public String iin;
    public String password;

    @StringRes
    public int validateIin() {
        if (isEmpty(iin)) {
            return R.string.field_error;
        } else if (StringUtils.length(iin) < Constants.MAX_LENGTH_IIN
                || StringUtils.length(iin) > Constants.MAX_LENGTH_IIN) {
            return R.string.iin_invalid_error;
        } else {
            return 0;
        }
    }

    @StringRes
    public int validatePwd() {
        if (isEmpty(password)) {
            return R.string.field_error;
        } else {
            return 0;
        }
    }
}

