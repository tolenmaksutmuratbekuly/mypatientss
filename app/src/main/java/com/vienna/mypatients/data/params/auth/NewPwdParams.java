package com.vienna.mypatients.data.params.auth;

import android.support.annotation.StringRes;

import com.vienna.mypatients.R;

import static android.text.TextUtils.isEmpty;
import static com.vienna.mypatients.utils.Constants.DEFAULT_MAX_LENGTH;
import static com.vienna.mypatients.utils.StringUtils.length;

public class NewPwdParams {
    public String password;
    public String password_confirmation;
    public String reset_password_token;

    @StringRes
    public int validatePwd() {
        if (isEmpty(password)) {
            return R.string.field_error;
        } else if (length(password) > DEFAULT_MAX_LENGTH) {
            return R.string.form_input_too_long;
        } else {
            return 0;
        }
    }

    public int validateConfirmPwd() {
        if (isEmpty(password_confirmation)) {
            return R.string.field_error;
        } else if (!password.equals(password_confirmation)) {
            return R.string.passwords_not_matches;
        } else if (length(password_confirmation) > DEFAULT_MAX_LENGTH) {
            return R.string.form_input_too_long;
        } else {
            return 0;
        }
    }
}
