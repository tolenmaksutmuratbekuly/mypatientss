package com.vienna.mypatients.data.params.my_patients;

public class FinishPostnatalParams {
    public String end_date;
    public String status;
    public int outcome;

    public FinishPostnatalParams(String end_date, String status, int outcome) {
        this.end_date = end_date;
        this.status = status;
        this.outcome = outcome;
    }
}