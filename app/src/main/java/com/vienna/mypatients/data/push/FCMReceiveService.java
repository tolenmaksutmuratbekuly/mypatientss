package com.vienna.mypatients.data.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vienna.mypatients.R;
import com.vienna.mypatients.ui.main.MainActivity;
import com.vienna.mypatients.utils.LOTimber;

import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;

public class FCMReceiveService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        LOTimber.d("MyFirebaseMsgService: " + remoteMessage.getFrom());
        //Check if the message contains data
        if (remoteMessage.getData().size() > 0) {
            LOTimber.d("MyFirebaseMsgService data=" + remoteMessage.getData());
            String changedNotificaiton = remoteMessage.getData().toString().replace("{message=", "").replace("}", EMPTY_STRING);
            sendNotification(changedNotificaiton);
        }

        //Check if the message contains notification
        if (remoteMessage.getNotification() != null) {
            LOTimber.d("MyFirebaseMsgService data=" + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String body) {
        Intent intent = MainActivity.alreadyAuthorized(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
    }
}

