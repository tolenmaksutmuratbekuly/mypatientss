package com.vienna.mypatients.data.repository.areas;

import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;

public interface AreasRepository {

    void setAPI(API api);

    Observable<ListOf<Area>> getAreasList(int number, int count);

    Observable<AreaDetail> getAreaDetail(String areaId);
}