package com.vienna.mypatients.data.repository.areas;

import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class AreasRepositoryImpl implements AreasRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<ListOf<Area>> getAreasList(int number, int count) {
        return api
                .getAreasList(number, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<AreaDetail> getAreaDetail(String areaId) {
        return api.getAreaDetail(areaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
