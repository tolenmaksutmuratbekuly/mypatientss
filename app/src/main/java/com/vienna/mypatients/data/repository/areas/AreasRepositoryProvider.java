package com.vienna.mypatients.data.repository.areas;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class AreasRepositoryProvider {
    private static AreasRepository repository;

    @NonNull
    public static AreasRepository provideRepository(API api){
        if(repository == null){
            repository =  new AreasRepositoryImpl();
        }
        repository.setAPI(api);
        return  repository;
    }

    public static void setRepository(AreasRepository repo) {
        repository = repo;
    }
}
