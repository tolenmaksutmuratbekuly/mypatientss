package com.vienna.mypatients.data.repository.auth;

import com.vienna.mypatients.data.models.auth.Auth;
import com.vienna.mypatients.data.models.auth.CheckPwdCode;
import com.vienna.mypatients.data.models.auth.ResetPwdPhone;
import com.vienna.mypatients.data.network.api.API;
import com.vienna.mypatients.data.params.auth.AuthParams;
import com.vienna.mypatients.data.params.auth.ForgotPwdParams;
import com.vienna.mypatients.data.params.auth.NewPwdParams;

import okhttp3.ResponseBody;
import rx.Observable;

public interface AuthRepository {

    void setAPI(API api);

    Observable<Auth> signIn(AuthParams params);

    Observable<ResetPwdPhone> resetPwdPhone(String iin);

    Observable<ResponseBody> resetPwd(ForgotPwdParams params);

    Observable<CheckPwdCode> checkPwdCode(ForgotPwdParams params);

    Observable<ResponseBody> newPassword(NewPwdParams params);
}