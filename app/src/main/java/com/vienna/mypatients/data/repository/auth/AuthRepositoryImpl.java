package com.vienna.mypatients.data.repository.auth;

import com.vienna.mypatients.data.models.auth.Auth;
import com.vienna.mypatients.data.models.auth.CheckPwdCode;
import com.vienna.mypatients.data.models.auth.ResetPwdPhone;
import com.vienna.mypatients.data.network.api.API;
import com.vienna.mypatients.data.params.auth.AuthParams;
import com.vienna.mypatients.data.params.auth.ForgotPwdParams;
import com.vienna.mypatients.data.params.auth.NewPwdParams;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class AuthRepositoryImpl implements AuthRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<Auth> signIn(AuthParams params) {
        return api
                .signIn(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResetPwdPhone> resetPwdPhone(String iin) {
        return api
                .resetPwdPhone(iin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> resetPwd(ForgotPwdParams params) {
        return api
                .resetPwd(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<CheckPwdCode> checkPwdCode(ForgotPwdParams params) {
        return api
                .checkPwdCode(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> newPassword(NewPwdParams params) {
        return api
                .newPassword(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}