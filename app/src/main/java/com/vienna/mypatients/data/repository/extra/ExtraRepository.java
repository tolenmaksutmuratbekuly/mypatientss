package com.vienna.mypatients.data.repository.extra;

import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.extra.Agreement;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;

public interface ExtraRepository {

    void setAPI(API api);

    Observable<Agreement> getAgreement();

    Observable<ListOf<Versions>> getVersionsList(int number, int count);
}