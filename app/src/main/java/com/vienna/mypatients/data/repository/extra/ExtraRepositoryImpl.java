package com.vienna.mypatients.data.repository.extra;

import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.extra.Agreement;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ExtraRepositoryImpl implements ExtraRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<Agreement> getAgreement() {
        return api
                .getAgreement()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ListOf<Versions>> getVersionsList(int number, int count) {
        return api
                .getVersionsList(number, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}