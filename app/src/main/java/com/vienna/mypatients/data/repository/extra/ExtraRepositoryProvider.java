package com.vienna.mypatients.data.repository.extra;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class ExtraRepositoryProvider {
    private static ExtraRepository repository;

    @NonNull
    public static ExtraRepository provideRepository(API api) {
        if (repository == null) {
            repository = new ExtraRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(ExtraRepository repo) {
        repository = repo;
    }
}