package com.vienna.mypatients.data.repository.patients;

import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.data.models.patients.PatientData;
import com.vienna.mypatients.data.network.api.API;
import com.vienna.mypatients.data.params.my_patients.FinishPostnatalParams;

import okhttp3.ResponseBody;
import rx.Observable;

public interface PatientsRepository {

    void setAPI(API api);

    Observable<ListOf<Patient>> getListOfPatients(int number,
                                                  int count,
                                                  String full_name,
                                                  String iin,
                                                  boolean prenatal_over,
                                                  boolean postnatal_over,
                                                  boolean transfer_over);

    Observable<ListOf<Children>> getListOfChildren(String patient_id);

    Observable<PatientData> getPatient(String patient_id);

    Observable<ResponseBody> finishPostnatal(String patient_id, String pregnancies_id, FinishPostnatalParams params);
}
