package com.vienna.mypatients.data.repository.patients;

import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.data.models.patients.PatientData;
import com.vienna.mypatients.data.network.api.API;
import com.vienna.mypatients.data.params.my_patients.FinishPostnatalParams;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class PatientsRepositoryImpl implements PatientsRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<ListOf<Patient>> getListOfPatients(int number,
                                                         int count,
                                                         String full_name,
                                                         String iin,
                                                         boolean prenatal_over,
                                                         boolean postnatal_over,
                                                         boolean transfer_over) {
        return api
                .getListOfPatients(number,
                                   count,
                                   full_name,
                                   iin,
                                   prenatal_over,
                                   postnatal_over,
                                   transfer_over)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ListOf<Children>> getListOfChildren(String patient_ids){
        return api.getListOfChildren(patient_ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<PatientData> getPatient(String patient_id) {
        return api.getPatient(patient_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> finishPostnatal(String patient_id, String pregnancies_id, FinishPostnatalParams params) {
        return api.finishPostnatal(patient_id, pregnancies_id, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
