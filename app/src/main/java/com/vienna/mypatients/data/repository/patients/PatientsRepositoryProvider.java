package com.vienna.mypatients.data.repository.patients;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class PatientsRepositoryProvider {
    private static PatientsRepository repository;

    @NonNull
    public static PatientsRepository provideRepository(API api){
        if(repository == null){
            repository =  new PatientsRepositoryImpl();
        }
        repository.setAPI(api);
        return  repository;
    }

    public static void setRepository(PatientsRepository repo) {
        repository = repo;
    }
}
