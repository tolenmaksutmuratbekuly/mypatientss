package com.vienna.mypatients.data.repository.questions;

import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;

public interface QuestionsRepository {

    void setAPI(API api);

    Observable<ListOf<Question>> getQuestions();
}
