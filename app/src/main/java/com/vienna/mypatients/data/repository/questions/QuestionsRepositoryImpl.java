package com.vienna.mypatients.data.repository.questions;

import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QuestionsRepositoryImpl implements QuestionsRepository{
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<ListOf<Question>> getQuestions() {
        return api
                .getQuestions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
