package com.vienna.mypatients.data.repository.questions;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class QuestionsRepositoryProvider {
    private static QuestionsRepository repository;

    @NonNull
    public static QuestionsRepository provideRepository(API api) {
        if (repository == null) {
            repository = new QuestionsRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(QuestionsRepository repo) {
        repository = repo;
    }
}
