package com.vienna.mypatients.data.repository.recommendations;

import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;

public interface RecommendationRepository {

    void setAPI(API api);

    Observable<ListOf<RecommendationCategory>> getRecommendationCategory(String kind);

    Observable<ListOf<Recommendation>> getRecommendations(String category_id);
}
