package com.vienna.mypatients.data.repository.recommendations;


import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.data.network.api.API;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RecommendationRepositoryImpl implements RecommendationRepository {
    private API api;

    @Override
    public void setAPI(API api) { this.api = api; }

    @Override
    public Observable<ListOf<RecommendationCategory>> getRecommendationCategory(String kind) {
        return api
                .getRecommendationCategory(kind)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ListOf<Recommendation>> getRecommendations(String category_id) {
        return api
                .getRecommendations(category_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
