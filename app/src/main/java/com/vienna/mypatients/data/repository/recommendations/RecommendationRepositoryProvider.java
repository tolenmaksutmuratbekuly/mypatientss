package com.vienna.mypatients.data.repository.recommendations;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class RecommendationRepositoryProvider {
    private static RecommendationRepository repository;

    @NonNull
    public static RecommendationRepository provideRepository(API api) {
        if (repository == null) {
            repository = new RecommendationRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(RecommendationRepository repo) {
        repository = repo;
    }
}
