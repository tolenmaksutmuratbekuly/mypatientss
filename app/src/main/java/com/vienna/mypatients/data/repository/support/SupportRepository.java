package com.vienna.mypatients.data.repository.support;

import com.vienna.mypatients.data.network.api.API;

public interface SupportRepository {

    void setAPI(API api);
}
