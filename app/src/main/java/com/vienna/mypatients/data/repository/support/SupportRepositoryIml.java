package com.vienna.mypatients.data.repository.support;

import com.vienna.mypatients.data.network.api.API;

public class SupportRepositoryIml implements SupportRepository{
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }
}
