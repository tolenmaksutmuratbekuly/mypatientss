package com.vienna.mypatients.data.repository.support;

import android.support.annotation.NonNull;

import com.vienna.mypatients.data.network.api.API;

public class SupportRepositoryProvider {
    private static SupportRepository repository;

    @NonNull
    public static SupportRepository provideRepository(API api) {
        if (repository == null) {
            repository = new SupportRepositoryIml();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(SupportRepository repo) {
        repository = repo;
    }
}
