package com.vienna.mypatients.data.repository.user;

import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.data.network.api.API;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import rx.Observable;

public interface UserRepository {

    void setAPI(API api);

    Observable<User> getProfile();

    Observable<User> setPhoto(MultipartBody.Part photo);
}