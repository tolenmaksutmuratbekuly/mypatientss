package com.vienna.mypatients.data.repository.user;

import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.data.network.api.API;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class UserRepositoryImpl implements UserRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<User> getProfile() {
        return api
                .getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<User> setPhoto(MultipartBody.Part photo) {
        return api
                .setPhoto(photo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}