package com.vienna.mypatients.mvp.areas;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.data.repository.areas.AreasRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

@InjectViewState
public class AreaDetailPresenter extends BaseMvpPresenter<AreaDetailView> {

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getAreaDetail(String areaId) {
        Subscription subscription = AreasRepositoryProvider.provideRepository(dataLayer.getApi())
                .getAreaDetail(areaId)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<AreaDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(AreaDetail areaDetail) {
                        if (areaDetail != null) {
                            getViewState().setAreaDetail(areaDetail.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

}
