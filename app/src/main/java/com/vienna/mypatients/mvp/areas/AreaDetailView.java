package com.vienna.mypatients.mvp.areas;

import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface AreaDetailView extends BaseMvpView {

    void setAreaDetail(AreaDetail.AreaDetailLocal areaDetail);
}
