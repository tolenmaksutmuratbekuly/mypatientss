package com.vienna.mypatients.mvp.areas;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.repository.areas.AreasRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.vienna.mypatients.utils.Constants.INITIAL_PAGE_NUMBER;
import static com.vienna.mypatients.utils.Constants.REQUEST_COUNT;

@InjectViewState
public class AreasPresenter extends BaseMvpPresenter<AreasView> {
    private int pagination;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        pagination = INITIAL_PAGE_NUMBER;
        getAreasList(false);
    }

    public void getAreasList(boolean is_load_more) {
        Subscription subscription = AreasRepositoryProvider.provideRepository(dataLayer.getApi())
                .getAreasList(is_load_more ? pagination + 1 : INITIAL_PAGE_NUMBER, REQUEST_COUNT)
                .doOnSubscribe(is_load_more ? () -> getViewState().showLoadingItemIndicator(true) : () -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(is_load_more ? () -> getViewState().showLoadingItemIndicator(false) : () -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Area>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Area> areasList) {
                        if (areasList != null && areasList.data != null && areasList.data.size() > 0) {
                            getViewState().setAreasList(areasList.data);
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }
}
