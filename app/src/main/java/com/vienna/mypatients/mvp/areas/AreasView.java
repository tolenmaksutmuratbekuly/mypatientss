package com.vienna.mypatients.mvp.areas;

import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface AreasView extends BaseMvpView {

    void setAreasList(List<Area> areasList);

    void showLoadingItemIndicator(boolean show);
}
