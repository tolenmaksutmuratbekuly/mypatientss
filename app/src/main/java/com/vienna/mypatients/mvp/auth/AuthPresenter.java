package com.vienna.mypatients.mvp.auth;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.extra.Agreement;
import com.vienna.mypatients.data.models.auth.Auth;
import com.vienna.mypatients.data.params.auth.AuthParams;
import com.vienna.mypatients.data.repository.extra.ExtraRepositoryProvider;
import com.vienna.mypatients.data.repository.auth.AuthRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;
import com.vienna.mypatients.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;

import static com.vienna.mypatients.utils.StringUtils.trim;

@InjectViewState
public class AuthPresenter extends BaseMvpPresenter<AuthView> {
    private AuthParams form;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        form = new AuthParams();
    }

    public void setIin(String iin) {
        form.iin = trim(iin);
    }

    public void setPwd(String password) {
        form.password = trim(password);
    }

    public void checkGeneralInfo() {
        boolean ok = validateIin();
        ok &= validatePwd();
        if (ok) {
            signIn();
        }
    }

    private void signIn() {
        Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                .signIn(form)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<Auth>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(Auth auth) {
                        getViewState().onAuthorizationSuccess();
                        /*
                        Access-Token:MNGPhftpGBVoPyzmfoMJ4g
                        Client:KweZC_xUgpSQZj-n6LGQ7g
                        Uid:900000000001
                         */
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onRegistrationClick() {
        getViewState().showRegistrationRequirementDialog();
    }

    public void onForgotPwdClick() {
        getViewState().openForgotPwdActivity();
    }

    public void getAgreement() {
        Subscription subscription = ExtraRepositoryProvider.provideRepository(dataLayer.getApi())
                .getAgreement()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<Agreement>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(Agreement agreement) {
                        if (agreement != null && StringUtils.length(agreement.getText()) > 0) {
                            getViewState().setAgreementContent(agreement.getText());
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Validation                                                           ///
    ///////////////////////////////////////////////////////////////////////////

    private boolean validateIin() {
        int errorRes = form.validateIin();
        if (errorRes != 0) {
            getViewState().showIinError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validatePwd() {
        int errorRes = form.validatePwd();
        if (errorRes != 0) {
            getViewState().showPwdError(errorRes);
        }
        return errorRes == 0;
    }
}