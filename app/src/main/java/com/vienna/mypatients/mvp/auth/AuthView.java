package com.vienna.mypatients.mvp.auth;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface AuthView extends BaseMvpView {
    void showIinError(@StringRes int errorRes);

    void showPwdError(@StringRes int errorRes);

    void showRegistrationRequirementDialog();

    void openForgotPwdActivity();

    void onAuthorizationSuccess();

    void setAgreementContent(String text);
}
