package com.vienna.mypatients.mvp.auth;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.auth.CheckPwdCode;
import com.vienna.mypatients.data.models.auth.ResetPwdPhone;
import com.vienna.mypatients.data.params.auth.ForgotPwdParams;
import com.vienna.mypatients.data.params.auth.NewPwdParams;
import com.vienna.mypatients.data.repository.auth.AuthRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.vienna.mypatients.utils.StringUtils.length;
import static com.vienna.mypatients.utils.StringUtils.trim;

@InjectViewState
public class ForgotPwdPresenter extends BaseMvpPresenter<ForgotPwdView> {
    private ForgotPwdParams form;
    private NewPwdParams formNewPwd;
    private String iin;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        form = new ForgotPwdParams();
        formNewPwd = new NewPwdParams();
    }

    public void setIin(String iin) {
        this.iin = iin;
        form.iin = trim(iin);
    }

    public void checkIin() {
        if (validateIin()) {
            resetPwdPhone();
        }
    }

    public void setCode(String code) {
        form.code = trim(code);
    }

    public void checkCode() {
        if (validateCode()) {
            checkPwdCode();
        }
    }

    public void setNewPwd(String newPwd) {
        formNewPwd.password = trim(newPwd);
    }

    public void setConfirmPwd(String confirmPwd) {
        formNewPwd.password_confirmation = trim(confirmPwd);
    }

    public void checkPasswords(String reset_password_token) {
        if (validatePwd() && validateConfirmPwd()) {
            formNewPwd.reset_password_token = reset_password_token;
            newPassword();
        }
    }

    // ====== Step - 1 =======
    private void resetPwdPhone() {
        Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                .resetPwdPhone(iin)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ResetPwdPhone>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ResetPwdPhone response) {
                        getViewState().showConfirmDialog(response.getPhone());
                    }
                });
        compositeSubscription.add(subscription);
    }

    // ====== Step - 2 =======
    public void resetPwd() {
        Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                .resetPwd(form)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ResponseBody response) {
                        getViewState().showCodeConfirmationView();
                    }
                });
        compositeSubscription.add(subscription);
    }

    // ====== Step - 3 =======
    private void checkPwdCode() {
        Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                .checkPwdCode(form)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<CheckPwdCode>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(CheckPwdCode response) {
                        if (length(response.getToken()) > 0) {
                            getViewState().showNewPwdViews(response.getToken());
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    // ====== Step - 4 =======
    private void newPassword() {
        Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                .newPassword(formNewPwd)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ResponseBody response) {
                        getViewState().showPasswordSuccessChangedDialog();
                    }
                });
        compositeSubscription.add(subscription);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Validation                                                           ///
    ///////////////////////////////////////////////////////////////////////////

    private boolean validateIin() {
        int errorRes = form.validateIin();
        if (errorRes != 0) {
            getViewState().showIinError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateCode() {
        int errorRes = form.validateCode();
        if (errorRes != 0) {
            getViewState().showCodeError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validatePwd() {
        int errorRes = formNewPwd.validatePwd();
        if (errorRes != 0) {
            getViewState().showNewPwdError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateConfirmPwd() {
        int errorRes = formNewPwd.validateConfirmPwd();
        if (errorRes != 0) {
            getViewState().showConfirmPwdError(errorRes);
        }
        return errorRes == 0;
    }
}