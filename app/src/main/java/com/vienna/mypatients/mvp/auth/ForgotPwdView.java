package com.vienna.mypatients.mvp.auth;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface ForgotPwdView extends BaseMvpView {

    void showIinError(@StringRes int errorRes);

    void showConfirmDialog(String phone);

    void showCodeConfirmationView();

    void showCodeError(int errorRes);

    void showNewPwdViews(String reset_password_token);

    void showNewPwdError(int errorRes);

    void showConfirmPwdError(int errorRes);

    void showPasswordSuccessChangedDialog();
}
