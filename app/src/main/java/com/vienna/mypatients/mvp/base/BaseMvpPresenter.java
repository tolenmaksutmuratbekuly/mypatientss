package com.vienna.mypatients.mvp.base;

import android.content.Context;

import com.arellomobile.mvp.MvpPresenter;
import com.vienna.mypatients.BuildConfig;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.network.error.RetrofitErrorHandler;
import com.vienna.mypatients.data.network.exceptions.APIException;
import com.vienna.mypatients.data.network.exceptions.ComingSoonException;
import com.vienna.mypatients.data.network.exceptions.ConnectionTimeOutException;
import com.vienna.mypatients.data.network.exceptions.NeedReAuthorizeException;
import com.vienna.mypatients.data.network.exceptions.UnknownException;
import com.vienna.mypatients.utils.StringUtils;

import rx.subscriptions.CompositeSubscription;

public abstract class BaseMvpPresenter<T extends BaseMvpView> extends MvpPresenter<T> {
    protected Context context;
    protected DataLayer dataLayer;
    protected CompositeSubscription compositeSubscription = new CompositeSubscription();

    protected void handleResponseError(Context context, Throwable e) {
        if (BuildConfig.IS_DEBUG) {
            e.printStackTrace();
        }
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (APIException e1) {
            getViewState().showRequestError(StringUtils.replaceNull(e1.getErrorDescr()));
        } catch (UnknownException e1) {
            getViewState().showRequestError(context.getString(R.string.server_error));
        } catch (ConnectionTimeOutException e1) {
            getViewState().showRequestError(context.getString(R.string.connection_error));
        } catch (NeedReAuthorizeException e1) {
            getViewState().onInvalidToken();
        } catch (ComingSoonException e1) {
            getViewState().appComingSoon();
        }
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}
