package com.vienna.mypatients.mvp.base;


import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {
    void showRequestError(String errorMessage);

    void showLoadingIndicator(boolean show);

    void onInvalidToken();

    void appComingSoon();
}
