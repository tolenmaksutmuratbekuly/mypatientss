package com.vienna.mypatients.mvp.language;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.extra.Agreement;
import com.vienna.mypatients.data.repository.extra.ExtraRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;
import com.vienna.mypatients.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;

@InjectViewState
public class AgreementPresenter extends BaseMvpPresenter<AgreementView> {
    private String agreementContent;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        getAgreement();
    }

    private void getAgreement() {
        Subscription subscription = ExtraRepositoryProvider.provideRepository(dataLayer.getApi())
                .getAgreement()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<Agreement>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(Agreement agreement) {
                        if (agreement != null && StringUtils.length(agreement.getText()) > 0) {
                            agreementContent = agreement.getText();
                            getViewState().setAgreementContent(agreement.getText());
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onAcceptClick() {
        if (StringUtils.length(agreementContent) > 0) {
            dataLayer.getPreferences().setShownAgreement(true);
            getViewState().openAuthActivity();
        }
    }
}