package com.vienna.mypatients.mvp.language;

import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface AgreementView extends BaseMvpView {
    void setAgreementContent(String text);

    void openAuthActivity();
}
