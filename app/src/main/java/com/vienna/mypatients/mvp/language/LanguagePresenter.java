package com.vienna.mypatients.mvp.language;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;
import com.vienna.mypatients.utils.LocaleUtils;

import static com.vienna.mypatients.utils.Constants.KEY_LANG_RU;
import static com.vienna.mypatients.utils.StringUtils.length;

@InjectViewState
public class LanguagePresenter extends BaseMvpPresenter<LanguageView> {

    private String selectedLang;
    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        String lang = dataLayer.getPreferences().getLanguage();
        if (length(lang) > 0) {
            getViewState().setLang(lang.equals(KEY_LANG_RU));
        } else {
            selectedLang = KEY_LANG_RU;
        }
    }

    public void setSelectedLang(String selectedLang) {
        this.selectedLang = selectedLang;
    }

    public void onChangeLanguageClick() {
        if (length(selectedLang) > 0) {
            LocaleUtils.updateResources(context, selectedLang);
            dataLayer.getPreferences().setLanguage(selectedLang);
        }
        getViewState().openAgreementActivity();
    }
}