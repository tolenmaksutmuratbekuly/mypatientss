package com.vienna.mypatients.mvp.language;

import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface LanguageView extends BaseMvpView {
    void setLang(boolean isRussian);

    void openAgreementActivity();
}
