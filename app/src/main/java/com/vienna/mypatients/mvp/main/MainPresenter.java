package com.vienna.mypatients.mvp.main;

import android.content.Context;
import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.data.repository.user.UserRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.vienna.mypatients.utils.StringUtils.isStringOk;

@InjectViewState
public class MainPresenter extends BaseMvpPresenter<MainView> {
    private Preferences preferences;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        preferences = dataLayer.getPreferences();
        getProfile();
    }

    private void getProfile() {
        Subscription subscription = UserRepositoryProvider.provideRepository(dataLayer.getApi())
                .getProfile()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(User response) {
                        if (response != null && response.data != null) {
                            setUserInfo(response.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    private void uploadProfilePhoto(File file) throws Exception {
        UserRepositoryProvider.provideRepository(dataLayer.getApi())
                .setPhoto(getParams(file))
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(User response) {
                        if (response != null && response.data != null) {
                            setUserInfo(response.data);
                        }
                        getViewState().profilePhotoSuccessUpdated();
                    }
                });
    }

    private void setUserInfo(User.Data response) {
        if (isStringOk(response.thumb)) {
            preferences.setProfileImage(response.thumb);
        }
        if (isStringOk(response.full_name)) {
            preferences.setFullName(response.full_name);
        }
        if (isStringOk(response.iin)) {
            preferences.setUserIIN(response.iin);
        }
        getViewState().setUser(response);
    }

    public void onAvatarChangeClick() {
        getViewState().openEditProfileImageDialog();
    }

    public void onSelectProfileImageClick(boolean isPermissionGrantedToStorage, boolean shouldShowRequestPermissionRationaleToStorage) {
        if (isPermissionGrantedToStorage) {
            getViewState().selectProfileImageFromIntent();
        } else {
            if (shouldShowRequestPermissionRationaleToStorage) {
                getViewState().showRequestPermissionRationaleToStorage();
            } else {
                getViewState().requestPermissionToStorage();
            }
        }
    }

    public void onTakePhotoClick() {
        getViewState().dispatchTakePictureIntent();
    }

    // Asking permissions
    public void onCropActivityResultOk(Uri uri) {
        if (uri != null) {
            try {
                File file = new File(uri.getPath());
                uploadProfilePhoto(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private MultipartBody.Part getParams(File file) {
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/*"), //MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData("photo", file.getName(), requestFile);
    }

    public void onRationaleToStorageAllowed() {
        getViewState().requestPermissionToStorage();
    }

    public void onPermissionGrantedToStorage() {
        getViewState().selectProfileImageFromIntent();
    }

    public void onPermissionNotGrantedToStorage(boolean shouldShowRequestPermissionRationaleToStorage) {
        if (shouldShowRequestPermissionRationaleToStorage) {
            getViewState().showRequestPermissionRationaleToStorage();
        } else {
            getViewState().showRequestPermissionDialogToStorage();
        }
    }

    public void onDialogToStorageAllowed() {
        getViewState().openSettingsActivity();
    }

    public void onActivityResultToStorage() {
        getViewState().selectProfileImageFromIntent();
    }
}