package com.vienna.mypatients.mvp.main;

import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface MainView extends BaseMvpView {
    void setUser(User.Data response);

    void openEditProfileImageDialog();

    void selectProfileImageFromIntent();

    void dispatchTakePictureIntent();

    void showRequestPermissionRationaleToStorage();

    void requestPermissionToStorage();

    void showRequestPermissionDialogToStorage();

    void openSettingsActivity();

    void profilePhotoSuccessUpdated();
}