package com.vienna.mypatients.mvp.patients;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.repository.patients.PatientsRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

@InjectViewState
public class ListOfChildrenPresenter extends BaseMvpPresenter<ListOfChildrenView> {

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getListOfChildren(String patient_id){
        Subscription subscription = PatientsRepositoryProvider.provideRepository(dataLayer.getApi())
                .getListOfChildren(patient_id)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Children>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Children> listOfChildren) {
                        if (listOfChildren != null && listOfChildren.data != null) {
                            getViewState().setListOfChildrens(listOfChildren.data);
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }

}
