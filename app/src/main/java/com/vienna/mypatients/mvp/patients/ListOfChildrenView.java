package com.vienna.mypatients.mvp.patients;

import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface ListOfChildrenView extends BaseMvpView {

    void setListOfChildrens(List<Children> childrens);
}
