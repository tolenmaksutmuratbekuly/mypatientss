package com.vienna.mypatients.mvp.patients;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.patients.Meta;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.data.repository.patients.PatientsRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.vienna.mypatients.utils.Constants.INITIAL_PAGE_NUMBER;
import static com.vienna.mypatients.utils.Constants.REQUEST_COUNT;

@InjectViewState
public class ListOfPatientPresenter extends BaseMvpPresenter<ListOfPatientsView> {
    private Meta meta;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        getListOfPatient(null, null, false, false, false, false);
    }

    public void getListOfPatient(String full_name,
                                 String iin,
                                 boolean prenatal_over,
                                 boolean postnatal_over,
                                 boolean transfer_over,
                                 boolean is_load_more) {

        Subscription subscription = PatientsRepositoryProvider.provideRepository(dataLayer.getApi())
                .getListOfPatients(is_load_more ? meta.current_page + 1 : INITIAL_PAGE_NUMBER,
                        is_load_more ? meta.count : REQUEST_COUNT,
                        full_name,
                        iin,
                        prenatal_over,
                        postnatal_over,
                        transfer_over)
                .doOnSubscribe(is_load_more ? () -> getViewState().showLoadingItemIndicator(true) : () -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(is_load_more ? () -> getViewState().showLoadingItemIndicator(false) : () -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Patient>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Patient> listOfPatient) {
                        if (listOfPatient != null && listOfPatient.data != null) {
                            if (listOfPatient.data.size() > 0) {
                                getViewState().setListOfPatients(listOfPatient.data);
                                meta = listOfPatient.meta;
                            }
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }
}
