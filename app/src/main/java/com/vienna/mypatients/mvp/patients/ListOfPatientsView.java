package com.vienna.mypatients.mvp.patients;

import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface ListOfPatientsView extends BaseMvpView {

    void setListOfPatients(List<Patient> patients);

    void showLoadingItemIndicator(boolean show);
}
