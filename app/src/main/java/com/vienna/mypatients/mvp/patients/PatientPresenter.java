package com.vienna.mypatients.mvp.patients;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.data.models.patients.PatientData;
import com.vienna.mypatients.data.repository.patients.PatientsRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

@InjectViewState
public class PatientPresenter extends BaseMvpPresenter<PatientView> {
    private Patient patient;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getPatient(String patient_id) {
        Subscription subscription = PatientsRepositoryProvider.provideRepository(dataLayer.getApi())
                .getPatient(patient_id)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<PatientData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(PatientData patientData) {
                        if (patientData != null && patientData.data != null) {
                            patient = patientData.data;
                            getViewState().setPatient(patientData.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onFinishPostnatalClick() {
        if (patient != null && patient.id != null
                && patient.pregnancy != null && patient.pregnancy.id != null) {
            getViewState().openFinishPostnatalActivity(patient.id, patient.pregnancy.id);
        } else {
            getViewState().showRequestError(context.getString(R.string.error_occurred));
        }
    }
}
