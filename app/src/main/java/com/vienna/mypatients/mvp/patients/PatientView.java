package com.vienna.mypatients.mvp.patients;

import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface PatientView extends BaseMvpView {

    void setPatient(Patient patient);

    void openFinishPostnatalActivity(String patient_id, String pregnancies_id);
}
