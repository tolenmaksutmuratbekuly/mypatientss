package com.vienna.mypatients.mvp.patients.finish_postnatal;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.params.my_patients.FinishPostnatalParams;
import com.vienna.mypatients.data.repository.patients.PatientsRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;
import com.vienna.mypatients.utils.DateUtils;

import java.util.Date;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.vienna.mypatients.utils.Constants.KEY_STATUS_COMPLETE;

@InjectViewState
public class FinishPostnatalPresenter extends BaseMvpPresenter<FinishPostnatalView> {
    private Context context;
    private DataLayer dataLayer;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private static final int MEDICAL_ABORT = 4;
    private static final int ABORT_PO_SOCIAL = 5;
    private static final int ABORT_PO_MEDICAL = 6;
    private static final int CRIMINAL_ABORT = 7;
    private static final int SELF_ABORT = 8;
    private static final int MINI_ABORT = 9;
    private static final int NO_REASON = 10;

    private static final int[] ABORT_REASONS = new int[]{MEDICAL_ABORT, ABORT_PO_SOCIAL, ABORT_PO_MEDICAL,
            CRIMINAL_ABORT, SELF_ABORT, MINI_ABORT, NO_REASON};

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    public void finishPostnatal(String patient_id, String pregnancies_id, Date date, int selectedItemPosition) {
        String dateStr = DateUtils.getFinishPostnatalDate(date);
        if (DateUtils.isAfterToday(date)) {
            getViewState().showRequestError(context.getString(R.string.choose_correct_date));
            return;
        }
        Subscription subscription = PatientsRepositoryProvider.provideRepository(dataLayer.getApi())
                .finishPostnatal(patient_id, pregnancies_id,
                        new FinishPostnatalParams(dateStr, KEY_STATUS_COMPLETE,
                                ABORT_REASONS[selectedItemPosition]))
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ResponseBody body) {
                        getViewState().successResponse();
                    }
                });
        compositeSubscription.add(subscription);

    }
}
