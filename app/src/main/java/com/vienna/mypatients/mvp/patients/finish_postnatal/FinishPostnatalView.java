package com.vienna.mypatients.mvp.patients.finish_postnatal;

import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface FinishPostnatalView extends BaseMvpView {

    void successResponse();
}
