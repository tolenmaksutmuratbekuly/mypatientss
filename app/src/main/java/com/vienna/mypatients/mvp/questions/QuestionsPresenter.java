package com.vienna.mypatients.mvp.questions;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.data.repository.questions.QuestionsRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;

@InjectViewState
public class QuestionsPresenter extends BaseMvpPresenter<QuestionsView>{

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getQuestions() {
        Subscription subscription = QuestionsRepositoryProvider.provideRepository(dataLayer.getApi())
                .getQuestions()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Question>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Question> questions) {
                        if (questions != null && questions.data != null) {
                            getViewState().setQuestions(questions.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }
}
