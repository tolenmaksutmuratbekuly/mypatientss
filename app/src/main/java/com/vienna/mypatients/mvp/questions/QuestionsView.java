package com.vienna.mypatients.mvp.questions;

import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface QuestionsView extends BaseMvpView {

    void setQuestions(List<Question> questions);
}
