package com.vienna.mypatients.mvp.recommendations;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.data.repository.recommendations.RecommendationRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;

@InjectViewState
public class RecommendationCategoryPresenter extends BaseMvpPresenter<RecommendationCategoryView> {

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getRecommendationCategory(String kind) {
        Subscription subscription = RecommendationRepositoryProvider.provideRepository(dataLayer.getApi())
                .getRecommendationCategory(kind)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<RecommendationCategory>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<RecommendationCategory> recommendationCategories) {
                        if (recommendationCategories != null && recommendationCategories.data != null) {
                            getViewState().setRecommendationCategory(recommendationCategories.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }
}
