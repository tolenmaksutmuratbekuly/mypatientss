package com.vienna.mypatients.mvp.recommendations;

import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface RecommendationCategoryView extends BaseMvpView{

    void setRecommendationCategory(List<RecommendationCategory> category);
}
