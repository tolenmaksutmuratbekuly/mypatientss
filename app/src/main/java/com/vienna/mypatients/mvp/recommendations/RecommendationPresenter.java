package com.vienna.mypatients.mvp.recommendations;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.data.repository.recommendations.RecommendationRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;

@InjectViewState
public class RecommendationPresenter extends BaseMvpPresenter<RecommendationView>{

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }

    public void getRecommendations(String category_id) {
        Subscription subscription = RecommendationRepositoryProvider.provideRepository(dataLayer.getApi())
                .getRecommendations(category_id)
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Recommendation>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Recommendation> recommendationCategories) {
                        if (recommendationCategories != null && recommendationCategories.data != null) {
                            getViewState().setRecommendation(recommendationCategories.data);
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }
}
