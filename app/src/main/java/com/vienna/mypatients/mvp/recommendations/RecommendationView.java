package com.vienna.mypatients.mvp.recommendations;

import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;

public interface RecommendationView extends BaseMvpView {

    void setRecommendation(List<Recommendation> recommendations);
}