package com.vienna.mypatients.mvp.support;

import android.content.Context;

import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

public class SupportPresenter extends BaseMvpPresenter<SupportView> {

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
    }
}
