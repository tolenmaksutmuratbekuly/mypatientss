package com.vienna.mypatients.mvp.support;

import com.vienna.mypatients.mvp.base.BaseMvpView;

public interface SupportView extends BaseMvpView {

    void sendMessage(String message);
}
