package com.vienna.mypatients.mvp.versions;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.models.common.ListOf;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.data.models.patients.Meta;
import com.vienna.mypatients.data.repository.extra.ExtraRepositoryProvider;
import com.vienna.mypatients.mvp.base.BaseMvpPresenter;

import rx.Subscriber;
import rx.Subscription;

import static com.vienna.mypatients.utils.Constants.INITIAL_PAGE_NUMBER;
import static com.vienna.mypatients.utils.Constants.REQUEST_COUNT;

@InjectViewState
public class VersionsPresenter extends BaseMvpPresenter<VersionsView> {
    private Meta meta;

    public void init(Context context, DataLayer dataLayer) {
        this.context = context;
        this.dataLayer = dataLayer;
        getVersionsList(false);
    }

    public void getVersionsList(boolean is_load_more) {
        Subscription subscription = ExtraRepositoryProvider.provideRepository(dataLayer.getApi())
                .getVersionsList(is_load_more ? meta.current_page + 1 : INITIAL_PAGE_NUMBER,
                        is_load_more ? meta.count : REQUEST_COUNT)
                .doOnSubscribe(is_load_more ? () -> getViewState().showLoadingItemIndicator(true) : () -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(is_load_more ? () -> getViewState().showLoadingItemIndicator(false) : () -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<ListOf<Versions>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(context, e);
                    }

                    @Override
                    public void onNext(ListOf<Versions> versionsList) {
                        if (versionsList != null && versionsList.data != null && versionsList.data.size() > 0) {
                            getViewState().setVersions(versionsList.data);
                            meta = versionsList.meta;
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }
}
