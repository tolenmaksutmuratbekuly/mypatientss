package com.vienna.mypatients.mvp.versions;

import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.mvp.base.BaseMvpView;

import java.util.List;


public interface VersionsView extends BaseMvpView {

    void setVersions(List<Versions> versions);

    void showLoadingItemIndicator(boolean show);
}
