package com.vienna.mypatients.ui.areas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.areas.AreaDetail;
import com.vienna.mypatients.mvp.areas.AreaDetailPresenter;
import com.vienna.mypatients.mvp.areas.AreaDetailView;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.ui.base.view_pager.ViewPagerAdapter;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.Constants.KEY_ID;
import static com.vienna.mypatients.utils.Constants.PAGE_TYPE_AREA_DETAIL_ACTIVITY;
import static com.vienna.mypatients.utils.Constants.TAB_COMPLAINS;
import static com.vienna.mypatients.utils.Constants.TAB_PATIENTS;
import static com.vienna.mypatients.utils.Constants.TAB_QUESTIONS;
import static com.vienna.mypatients.utils.StringUtils.isStringOk;

public class AreaDetailActivity extends BaseActivity implements AreaDetailView {
    @InjectPresenter
    AreaDetailPresenter mvpPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tv_area_name) TextView tv_area_name;
    @BindView(R.id.tv_medical_office) TextView tv_medical_office;
    @BindView(R.id.tv_principal_doctor) TextView tv_principal_doctor;
    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.pager) ViewPager viewPager;
    private ViewPagerAdapter adapter;

    //    @BindView(R.id.tv_sum_of_patients) TextView tv_sum_of_patients;
    public static Intent getIntent(Context context, String areaId) {
        Intent intent = new Intent(context, AreaDetailActivity.class);
        intent.putExtra(KEY_ID, areaId);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_area_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);

        if (getIntent() != null) {
            String id = getIntent().getStringExtra(KEY_ID);
            mvpPresenter.getAreaDetail(id);
        }

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        configureViewPager();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void configureViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        tabs.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.btn_patients)
    void onPatientsClick() {
        ToastUtils.showToast(this, "Open Patients activity");
    }

    @OnClick(R.id.btn_profile)
    void onDoctorClick() {
        ToastUtils.showToast(this, "Open Doctor\'s profile");
    }

    ///////////////////////////////////////////////////////////////////////////
    // AreaDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setAreaDetail(AreaDetail.AreaDetailLocal areaDetail) {
        if (isStringOk(areaDetail.name)) tv_area_name.setText(areaDetail.name);
        if (areaDetail.organization != null) {
            if (isStringOk(areaDetail.organization.name)) {
                String boldWord = getString(R.string.medical_office);
                String wholeWord = boldWord + " " + areaDetail.organization.name;
                tv_medical_office.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));
            }
        }

        if (areaDetail.principal_doctor != null) {
            if (isStringOk(areaDetail.principal_doctor.name)) {
                String boldWord = getString(R.string.fio_principal_doctor);
                String wholeWord = boldWord + " " + areaDetail.principal_doctor.name;
                tv_principal_doctor.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));
            }
        }

        if (areaDetail.area_statistic != null) {
            adapter.addFrag(InfoFragment.newInstance(areaDetail.area_statistic, TAB_COMPLAINS, PAGE_TYPE_AREA_DETAIL_ACTIVITY), getString(R.string.tab_complains));
            adapter.addFrag(InfoFragment.newInstance(areaDetail.area_statistic, TAB_PATIENTS, PAGE_TYPE_AREA_DETAIL_ACTIVITY), getString(R.string.patients));
            adapter.addFrag(InfoFragment.newInstance(areaDetail.area_statistic, TAB_QUESTIONS, PAGE_TYPE_AREA_DETAIL_ACTIVITY), getString(R.string.tab_questions));
            adapter.notifyDataSetChanged();
//            tv_sum_of_patients.setText(getString(R.string.prof_sum_patients,
//                    String.valueOf(areaDetail.area_statistic.getAllPatientsSum())));
        }
    }
}
