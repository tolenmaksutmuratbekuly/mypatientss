package com.vienna.mypatients.ui.areas;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.mvp.areas.AreasPresenter;
import com.vienna.mypatients.mvp.areas.AreasView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;
import com.vienna.mypatients.utils.recycler_view.EndlessScrollListener;

import java.util.List;

import butterknife.BindView;

public class AreasActivity extends BaseDrawerNavActivity implements AreasView {
    @InjectPresenter
    AreasPresenter mvpPresenter;
    private AreasAdapter mAdapter;

    @BindView(R.id.recycler_view) RecyclerView recycler_view;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recyclerview;
    }

    @Override
    protected int getActivityId() {
        return DN_AREAS_ACTIVITY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        actionBar.setDisplayShowTitleEnabled(true);
        toolbar.setTitle(getString(R.string.my_areas));
        configureRecyclerView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    private void configureRecyclerView() {
        mAdapter = new AreasAdapter(this);
        mAdapter.setCallback(area -> startActivity(AreaDetailActivity.getIntent(this, area.id)));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
        recycler_view.addOnScrollListener(new EndlessScrollListener(recycler_view, 1) {
            @Override
            public void onRequestMoreItems() {
                if (!mAdapter.getLoading()) {
                    mvpPresenter.getAreasList(true);
                }
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // AreasView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setAreasList(List<Area> areas) {
        mAdapter.addAreasList(areas);
    }

    @Override
    public void showLoadingItemIndicator(boolean show) {
        mAdapter.setLoading(show);
    }
}