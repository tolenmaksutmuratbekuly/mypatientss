package com.vienna.mypatients.ui.areas;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;
import static com.vienna.mypatients.utils.StringUtils.isStringOk;

class AreasAdapter extends RecyclerViewBaseAdapter {
    private static final int AREA_LAYOUT_ID = R.layout.adapter_areas;

    private Context context;
    private List<Area> data;
    private Callback callback;
    private boolean loading;

    AreasAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    boolean getLoading() {
        return loading;
    }

    void clearAreas() {
        data.clear();
        notifyDataSetChanged();
    }

    void addAreasList(List<Area> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case AREA_LAYOUT_ID:
                return new AreaViewHolder(inflate(parent, AREA_LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(parent, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else {
            return AREA_LAYOUT_ID;
        }
    }

    @Override
    public int getItemCount() {
        int count = data.size();
        if (loading) {
            count += 1;
        }
        return count;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case AREA_LAYOUT_ID:
                ((AreaViewHolder) holder).bind(data.get(position), position);
                break;
        }
    }

    class AreaViewHolder extends MainViewHolder {
        @BindView(R.id.tv_area_name) TextView tv_area_name;

        Area bindedObject;
        int bindedPosition;
        RowViewHolder v1, v2, v3, v4, v5;

        AreaViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v1 = new RowViewHolder(v.findViewById(R.id.v1));
            v2 = new RowViewHolder(v.findViewById(R.id.v2));
            v3 = new RowViewHolder(v.findViewById(R.id.v3));
            v4 = new RowViewHolder(v.findViewById(R.id.v4));
            v5 = new RowViewHolder(v.findViewById(R.id.v5));
        }

        void bind(Area area, int position) {
            bindedObject = area;
            bindedPosition = position;

            if (area == null) {
                return;
            }

            if (isStringOk(area.name)) tv_area_name.setText(area.name);
            else tv_area_name.setText(EMPTY_STRING);

            if (area.statistic != null) {
                v1.bindHolder(R.string.prof_area_sum_patients, String.valueOf(area.statistic.getAllPatientsSum()));
                v2.bindHolder(R.string.prof_area_sum_complains_without, String.valueOf(area.statistic.getUnadvisedComplains()));
                v3.bindHolder(R.string.prof_area_sum_answer_without, String.valueOf(area.statistic.getUnansweredQuestions()));
                v4.bindHolder(R.string.prof_sum_patients_till_41, String.valueOf(area.statistic.getPrenatalOver()));
                v5.bindHolder(R.string.prof_sum_patients_after_42, String.valueOf(area.statistic.getPostnatalOver()));
            }
        }

        @OnClick(R.id.ll_content)
        void onItemViewClick() {
            if (callback != null) {
                callback.onItemClick(bindedObject);
            }
        }
    }

    interface Callback {
        void onItemClick(Area area);
    }
}
