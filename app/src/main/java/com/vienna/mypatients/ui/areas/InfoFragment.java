package com.vienna.mypatients.ui.areas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.profile.Statistic;

import org.parceler.Parcels;

import static com.vienna.mypatients.utils.Constants.KEY_AREA;
import static com.vienna.mypatients.utils.Constants.KEY_PAGE_TYPE;
import static com.vienna.mypatients.utils.Constants.KEY_TYPE;
import static com.vienna.mypatients.utils.Constants.PAGE_TYPE_AREA_DETAIL_ACTIVITY;
import static com.vienna.mypatients.utils.Constants.PAGE_TYPE_MAIN_ACTIVITY;
import static com.vienna.mypatients.utils.Constants.TAB_COMPLAINS;
import static com.vienna.mypatients.utils.Constants.TAB_PATIENTS;
import static com.vienna.mypatients.utils.Constants.TAB_QUESTIONS;
import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;

public class InfoFragment extends Fragment {
    private Statistic area_statistic;
    private int type, pageType;

    public static InfoFragment newInstance(Statistic area_statistic, int type, int pageType) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_AREA, Parcels.wrap(area_statistic));
        args.putInt(KEY_TYPE, type);
        args.putInt(KEY_PAGE_TYPE, pageType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(KEY_AREA) && bundle.containsKey(KEY_TYPE)) {
            area_statistic = Parcels.unwrap(bundle.getParcelable(KEY_AREA));
            type = bundle.getInt(KEY_TYPE);
            pageType = bundle.getInt(KEY_PAGE_TYPE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgm_area_infos, container, false);
        RowViewHolder v1 = new RowViewHolder(view.findViewById(R.id.v1));
        RowViewHolder v2 = new RowViewHolder(view.findViewById(R.id.v2));
        RowViewHolder v3 = new RowViewHolder(view.findViewById(R.id.v3));
        RowViewHolder v4 = new RowViewHolder(view.findViewById(R.id.v4));
        RowViewHolder v5 = new RowViewHolder(view.findViewById(R.id.v5));

        switch (type) {
            case TAB_COMPLAINS:
                if (pageType == PAGE_TYPE_MAIN_ACTIVITY) {
                    v1.bindHolder(R.string.prof_sum_complains, String.valueOf(area_statistic.getComplaintsAdvised()));
                    v2.bindHolder(R.string.prof_sum_complains_without, String.valueOf(area_statistic.getComplaintsUnadvised()));
                    v3.bindHolder(0, EMPTY_STRING);
                    v4.bindHolder(0, EMPTY_STRING);
                    v5.bindHolder(0, EMPTY_STRING);

                } else if (pageType == PAGE_TYPE_AREA_DETAIL_ACTIVITY) {
                    v1.bindHolder(R.string.complaints_sum, String.valueOf(area_statistic.getAllComplaints()));
                    v2.bindHolder(R.string.complaints_without_recommendation, String.valueOf(area_statistic.getComplaintsUnadvised()));
                    v3.bindHolder(0, EMPTY_STRING);
                    v4.bindHolder(0, EMPTY_STRING);
                    v5.bindHolder(0, EMPTY_STRING);
                }
                break;
            case TAB_PATIENTS:
                if (pageType == PAGE_TYPE_MAIN_ACTIVITY) {
                    v1.bindHolder(R.string.prof_sum_patients, String.valueOf(area_statistic.getAllPatientsSum()));
                    v2.bindHolder(R.string.prof_sum_patients_prenatal, String.valueOf(area_statistic.getPrenatalFewer()));
                    v3.bindHolder(R.string.prof_sum_patients_postnatal, String.valueOf(area_statistic.getPostnatalFewer()));
                    v4.bindHolder(R.string.prof_sum_patients_require, String.valueOf(area_statistic.getPatientsOver()));
                    v5.bindHolder(R.string.prof_sum_patients_removed, String.valueOf(area_statistic.getPatientsComplete()));

                } else if (pageType == PAGE_TYPE_AREA_DETAIL_ACTIVITY) {
                    v1.bindHolder(R.string.prof_sum_patients_till_now, String.valueOf(area_statistic.getPrenatal()));
                    v2.bindHolder(R.string.prof_sum_patients_after_now, String.valueOf(area_statistic.getPostnatal()));
                    v3.bindHolder(R.string.prof_patients_till_41, String.valueOf(area_statistic.getPrenatalOver()));
                    v4.bindHolder(R.string.prof_patients_after_42, String.valueOf(area_statistic.getPostnatalOver()));
                    v5.bindHolder(0, EMPTY_STRING);
                }
                break;
            case TAB_QUESTIONS:
                if (pageType == PAGE_TYPE_MAIN_ACTIVITY) {
                    v1.bindHolder(R.string.prof_sum_answer, String.valueOf(area_statistic.getAnswered()));
                    v2.bindHolder(R.string.prof_sum_answer_without, String.valueOf(area_statistic.getUnansered()));
                    v3.bindHolder(0, EMPTY_STRING);
                    v4.bindHolder(0, EMPTY_STRING);
                    v5.bindHolder(0, EMPTY_STRING);

                } else if (pageType == PAGE_TYPE_AREA_DETAIL_ACTIVITY) {
                    v1.bindHolder(R.string.questions_sum, String.valueOf(area_statistic.getAllQuestions()));
                    v2.bindHolder(R.string.questions_without_recommendation, String.valueOf(area_statistic.getUnanswered()));
                    v3.bindHolder(0, EMPTY_STRING);
                    v4.bindHolder(0, EMPTY_STRING);
                    v5.bindHolder(0, EMPTY_STRING);
                }
                break;
        }
        return view;
    }
}