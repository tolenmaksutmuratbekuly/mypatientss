package com.vienna.mypatients.ui.areas;

import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vienna.mypatients.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vienna.mypatients.utils.StringUtils.length;

public class RowViewHolder {
    private ViewGroup rowView;
    @BindView(R.id.tv_title) TextView tv_title;
    @BindView(R.id.tv_count) TextView tv_count;

    RowViewHolder(View view) {
        ButterKnife.bind(this, view);
        rowView = (ViewGroup) view;
    }

    void bindHolder(@StringRes int titleRes, String count) {
        if (titleRes == 0 && length(count) == 0) {
            rowView.setVisibility(View.GONE);
            tv_title.setText(null);
            tv_count.setText(null);
        } else {
            rowView.setVisibility(View.VISIBLE);
            tv_title.setText(titleRes);
            tv_count.setText(count);
        }
    }
}
