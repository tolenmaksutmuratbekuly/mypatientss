package com.vienna.mypatients.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.vienna.mypatients.R;
import com.vienna.mypatients.mvp.auth.AuthPresenter;
import com.vienna.mypatients.mvp.auth.AuthView;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.ui.main.MainActivity;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.views.dialogs.CommonDialog;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.vienna.mypatients.utils.ContextUtils.clearFocusFromAllViews;
import static com.vienna.mypatients.utils.ContextUtils.hideSoftKeyboard;

public class AuthActivity extends BaseActivity implements AuthView {
    @InjectPresenter
    AuthPresenter mvpPresenter;
    @BindView(R.id.et_iin) MaterialEditText et_iin;
    @BindView(R.id.et_pwd) MaterialEditText et_pwd;
    @BindView(R.id.tv_forgot_pwd) TextView tv_forgot_pwd;
    @BindView(R.id.tv_registration) TextView tv_registration;
    @BindView(R.id.tv_license) TextView tv_license;
    @BindView(R.id.tv_too_vienna) TextView tv_too_vienna;

    private CommonDialog dialog;
    private String agreementContent;

    public static Intent newLogoutIntent(Context context) {
        return new Intent(context, AuthActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, AuthActivity.class);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_auth;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);

        dialog = new CommonDialog(this);
        configureViews();
    }

    private void configureViews() {
        tv_forgot_pwd.setPaintFlags(tv_forgot_pwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_registration.setPaintFlags(tv_registration.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_license.setPaintFlags(tv_license.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_too_vienna.setPaintFlags(tv_too_vienna.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    @OnClick(R.id.btn_login)
    void onLoginClick() {
        clearFocusFromAllViews(fl_container);
        hideSoftKeyboard(fl_container);
        mvpPresenter.setIin(et_iin.getText().toString());
        mvpPresenter.setPwd(et_pwd.getText().toString());
        mvpPresenter.checkGeneralInfo();
    }

    @OnEditorAction(R.id.et_pwd)
    boolean onPwdInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(et_pwd);
            clearFocusFromAllViews(fl_container);
            return true;
        }
        return false;
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    @OnClick(R.id.tv_registration)
    void onRegistrationClick() {
        mvpPresenter.onRegistrationClick();
    }

    @OnClick(R.id.tv_forgot_pwd)
    void onForgotPwdClick() {
        mvpPresenter.onForgotPwdClick();
    }

    @OnClick(R.id.tv_license)
    void onAgreementClick() {
        if (StringUtils.length(agreementContent) > 0) {
            showAgreementDialog();
        } else {
            mvpPresenter.getAgreement();
        }
    }

    void showAgreementDialog() {
        dialog.showDialogTitleDescSingleBtn(
                getString(R.string.license_agreement),
                agreementContent,
                getString(R.string.close), true, true);
    }

    @Override
    public void onBackPressed() {
        dialog.setCallbackPositiveNegative(new CommonDialog.CallbackPositiveNegative() {
            @Override
            public void onClickPositive() {
                finish();
            }

            @Override
            public void onClickNegative() {
            }
        });
        dialog.showDialogPositiveNegative(getString(R.string.logout_title),
                getString(R.string.logout_from_application), getString(R.string.yes), getString(R.string.no), true, true);
    }

    ///////////////////////////////////////////////////////////////////////////
    // AuthView implementation                                              ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showIinError(@StringRes int errorRes) {
        showInputFieldError(et_iin, errorRes);
    }

    @Override
    public void showPwdError(@StringRes int errorRes) {
        showInputFieldError(et_pwd, errorRes);
    }

    @Override
    public void showRegistrationRequirementDialog() {
        dialog.showDialogTitleDescSingleBtn(
                getString(R.string.label_registration),
                getString(R.string.registration_info),
                getString(R.string.ok), true, true);
    }

    @Override
    public void openForgotPwdActivity() {
        startActivity(ForgotPwdActivity.getIntent(this));
    }

    @Override
    public void onAuthorizationSuccess() {
        startActivity(MainActivity.getIntent(this));
    }

    @Override
    public void setAgreementContent(String agreementContent) {
        this.agreementContent = agreementContent;
        showAgreementDialog();
    }
}