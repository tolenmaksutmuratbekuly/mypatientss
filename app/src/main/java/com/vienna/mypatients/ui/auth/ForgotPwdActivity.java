package com.vienna.mypatients.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.vienna.mypatients.R;
import com.vienna.mypatients.mvp.auth.ForgotPwdPresenter;
import com.vienna.mypatients.mvp.auth.ForgotPwdView;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.views.dialogs.CommonDialog;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.vienna.mypatients.utils.ContextUtils.clearFocusFromAllViews;
import static com.vienna.mypatients.utils.ContextUtils.hideSoftKeyboard;

public class ForgotPwdActivity extends BaseActivity implements ForgotPwdView {
    @InjectPresenter
    ForgotPwdPresenter mvpPresenter;
    @BindView(R.id.img_logo_human) ImageView img_logo_human;
    @BindView(R.id.img_logo_auth) ImageView img_logo_auth;
    @BindView(R.id.et_iin) MaterialEditText et_iin;
    @BindView(R.id.et_code) MaterialEditText et_code;
    @BindView(R.id.et_pwd) MaterialEditText et_pwd;
    @BindView(R.id.v_pwd_divider) View v_pwd_divider;
    @BindView(R.id.et_pwd_confirm) MaterialEditText et_pwd_confirm;
    @BindView(R.id.btn_continue) Button btn_continue;

    private CommonDialog dialog;
    private String reset_password_token;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_forgot_pwd;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ForgotPwdActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);

        dialog = new CommonDialog(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    @OnClick(R.id.btn_continue)
    void onContinueClick() {
        clearFocusFromAllViews(fl_container);
        hideSoftKeyboard(fl_container);

        if (et_iin.getVisibility() == View.VISIBLE) {
            mvpPresenter.setIin(et_iin.getText().toString());
            mvpPresenter.checkIin();
        } else if (et_code.getVisibility() == View.VISIBLE) {
            mvpPresenter.setCode(et_code.getText().toString());
            mvpPresenter.checkCode();
        } else if (et_pwd.getVisibility() == View.VISIBLE &&
                et_pwd_confirm.getVisibility() == View.VISIBLE) {
            mvpPresenter.setNewPwd(et_pwd.getText().toString());
            mvpPresenter.setConfirmPwd(et_pwd_confirm.getText().toString());
            mvpPresenter.checkPasswords(reset_password_token);
        }
    }

    @OnEditorAction({R.id.et_iin, R.id.et_code, R.id.et_pwd, R.id.et_pwd_confirm})
    boolean onInputsAction(MaterialEditText editText, int action) {
        if (action == EditorInfo.IME_ACTION_DONE) {
            hideSoftKeyboard(editText);
            clearFocusFromAllViews(fl_container);
            return true;
        }
        return false;
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    ///////////////////////////////////////////////////////////////////////////
    // ForgotPwdView implementation                                         ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showIinError(@StringRes int errorRes) {
        showInputFieldError(et_iin, errorRes);
    }

    @Override
    public void showConfirmDialog(String phone) {
        dialog.setCallbackPositiveNegative(new CommonDialog.CallbackPositiveNegative() {
            @Override
            public void onClickPositive() {
                mvpPresenter.resetPwd();
            }

            @Override
            public void onClickNegative() {

            }
        });
        dialog.showDialogPositiveNegative(
                getString(R.string.confirmation),
                getString(R.string.dialog_pwd_description, phone),
                getString(R.string.yes),
                getString(R.string.cancellation),
                true, true);
    }

    @Override
    public void showCodeConfirmationView() {
        et_iin.setVisibility(View.GONE);
        img_logo_human.setVisibility(View.GONE);
        img_logo_auth.setVisibility(View.VISIBLE);
        et_code.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCodeError(int errorRes) {
        showInputFieldError(et_code, errorRes);
    }

    @Override
    public void showNewPwdViews(String reset_password_token) {
        et_iin.setVisibility(View.GONE);
        img_logo_human.setVisibility(View.GONE);
        img_logo_auth.setVisibility(View.VISIBLE);
        et_code.setVisibility(View.GONE);
        et_pwd.setVisibility(View.VISIBLE);
        v_pwd_divider.setVisibility(View.VISIBLE);
        et_pwd_confirm.setVisibility(View.VISIBLE);
        btn_continue.setText(getString(R.string.save));
        this.reset_password_token = reset_password_token;
    }

    @Override
    public void showNewPwdError(int errorRes) {
        showInputFieldError(et_pwd, errorRes);
    }

    @Override
    public void showConfirmPwdError(int errorRes) {
        showInputFieldError(et_pwd_confirm, errorRes);
    }

    @Override
    public void showPasswordSuccessChangedDialog() {
        dialog.setCallbackSingleBtn(this::finish);
        dialog.showDialogTitleDescSingleBtn(getString(R.string.pwd_success_changed),
                getString(R.string.use_for_auth), getString(R.string.ok), true, true);
    }
}