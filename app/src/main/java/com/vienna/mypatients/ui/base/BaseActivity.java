package com.vienna.mypatients.ui.base;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.data.cache.preferences.PreferencesProvider;
import com.vienna.mypatients.mvp.base.BaseMvpView;
import com.vienna.mypatients.utils.LocaleUtils;
import com.vienna.mypatients.utils.SnackbarUtils;
import com.vienna.mypatients.views.dialogs.CommonDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {
    protected DataLayer dataLayer;
    @BindView(R.id.fl_container) protected FrameLayout fl_container;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences preferences = new PreferencesProvider(this);
        LocaleUtils langUtil = new LocaleUtils(preferences, this);
        langUtil.fixLocale(langUtil.getLocale());
        if (getLayoutResourceId() != -1) setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        dataLayer = DataLayer.getInstance(this);
    }

    protected abstract int getLayoutResourceId();

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_container, errorMessage);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onInvalidToken() {
        InvalidToken.wipeOut(this, dataLayer);
    }

    @Override
    public void appComingSoon() {
        CommonDialog commonDialog = new CommonDialog(this);
        commonDialog.setCallbackSingleBtn(this::finish);
        commonDialog.showDialogTitleDescSingleBtn(
                getString(R.string.coming_soon_title),
                getString(R.string.coming_soon_description),
                getString(R.string.ok),
                true, true);
    }
}
