package com.vienna.mypatients.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.data.cache.preferences.PreferencesProvider;
import com.vienna.mypatients.data.network.RetrofitServiceGenerator;
import com.vienna.mypatients.data.network.api.API;

public class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment {

    protected API api;
    protected Preferences preferences;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new PreferencesProvider(getContext());

        RetrofitServiceGenerator serviceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        api = serviceGenerator.createService(API.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    protected void setBottomSheetCallback(View v) {
        BottomSheetBehavior mBottomSheetBehavior = BottomSheetBehavior.from(((View) v.getParent()));
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        ((View) v.getParent()).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_dialog));
        //another way to do above thing
//        CoordinatorLayout.LayoutParams layoutParams =
//                (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
//        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
//        if (behavior != null && behavior instanceof BottomSheetBehavior) {
//            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
//        }
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    protected void setActivityResult(Bundle bundle) {
        Intent intent = new Intent();
        intent.putExtras(bundle);

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismiss();
    }
}