package com.vienna.mypatients.ui.base;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.mvp.base.BaseMvpView;
import com.vienna.mypatients.ui.areas.AreasActivity;
import com.vienna.mypatients.ui.auth.AuthActivity;
import com.vienna.mypatients.ui.main.MainActivity;
import com.vienna.mypatients.ui.patients.ListOfPatientActivty;
import com.vienna.mypatients.ui.questions.QuestionsActivity;
import com.vienna.mypatients.ui.recommendations.RecommendationTypeActivity;
import com.vienna.mypatients.ui.support.SupportActivity;
import com.vienna.mypatients.ui.versions.VersionsActivity;
import com.vienna.mypatients.utils.DeviceUtils;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.LocaleUtils;
import com.vienna.mypatients.utils.SnackbarUtils;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.views.dialogs.CommonDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.StringUtils.menuIcons;

public abstract class BaseDrawerNavActivity extends MvpAppCompatActivity implements BaseMvpView {
    protected int DN_MAIN_ACTIVITY = 0;
    protected int DN_LIST_OF_PATIENT_ACTIVITY = 1;
    protected int DN_AREAS_ACTIVITY = 2;
    protected int DN_RECOMMENDATIONS_ACTIVITY = 4;
    protected int DN_QUESTIONS = 5;
    protected int DN_SUPPORT_ACTIVITY = 10;
    protected int DN_VERSIONS_ACTIVITY = 11;

    protected DataLayer dataLayer;
    protected Preferences preferences;
    protected ActionBar actionBar;
    private int width;
    private HeaderViewHolder headerViewHolder;

    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.fl_container) protected FrameLayout fl_container;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;
    @BindView(R.id.drawer_layout) DrawerLayout drawer_layout;
    @BindView(R.id.lv_left_drawer) ListView lv_left_drawer;
    @BindView(R.id.ll_left_drawer) LinearLayout ll_left_drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataLayer = DataLayer.getInstance(this);
        preferences = dataLayer.getPreferences();
        LocaleUtils langUtil = new LocaleUtils(preferences, this);
        langUtil.fixLocale(langUtil.getLocale());
        if (getLayoutResourceId() != -1)
            setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        width = DeviceUtils.getScreenWidthHeight(this)[0];
        configureDrawerNavigation();
    }

    protected abstract int getLayoutResourceId();

    protected abstract int getActivityId();

    public void configureDrawerNavigation() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_dn);
        }
        configureListView();
    }

    private void configureListView() {
        String[] menuTitles = StringUtils.getDnMenuList(this);
        ArrayList<MenuList> menuList = new ArrayList<>();
        for (int i = 0; i < menuIcons.length; i++) {
            menuList.add(new MenuList(menuTitles[i], menuIcons[i]));
        }

        MenuListAdapter menuListAdapter = new MenuListAdapter(this, menuList);
        lv_left_drawer.setOnItemClickListener((parent, view, position, id) -> {
            position -= lv_left_drawer.getHeaderViewsCount();
            if (getActivityId() == position) {
                drawer_layout.closeDrawer(ll_left_drawer);
                return;
            }

            drawer_layout.closeDrawer(ll_left_drawer);
            if (position == DN_MAIN_ACTIVITY) {
                startActivity(new Intent(this, MainActivity.class));
            } else if (position == DN_LIST_OF_PATIENT_ACTIVITY) {
                startActivity(ListOfPatientActivty.getIntent(this));
            } else if (position == DN_AREAS_ACTIVITY) {
                startActivity(new Intent(this, AreasActivity.class));
            } else if (position == DN_RECOMMENDATIONS_ACTIVITY) {
                startActivity(new Intent(this, RecommendationTypeActivity.class));
            } else if (position == DN_QUESTIONS) {
                startActivity(new Intent(this, QuestionsActivity.class));
            } else if (position == DN_SUPPORT_ACTIVITY) {
                startActivity(new Intent(this, SupportActivity.class));
            } else if (position == DN_VERSIONS_ACTIVITY) {
                startActivity(new Intent(this, VersionsActivity.class));
            } else if (position == menuListAdapter.getCount() - 1) {
                onLogoutClick();
            }
        });

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.inc_drawer_header, lv_left_drawer, false);
        headerViewHolder = new HeaderViewHolder(this, header);
        bindHeaderViewHolder();
        lv_left_drawer.addHeaderView(header, null, false);
        lv_left_drawer.setAdapter(menuListAdapter);
        ll_left_drawer.getLayoutParams().width = (int) (width * 0.80);
    }

    protected void onLogoutClick() {
        CommonDialog dialogUtils = new CommonDialog(this);
        dialogUtils.setCallbackPositiveNegative(new CommonDialog.CallbackPositiveNegative() {
            @Override
            public void onClickPositive() {
                dataLayer.wipeOut();
                startActivity(AuthActivity.newLogoutIntent(getBaseContext()));
            }

            @Override
            public void onClickNegative() {
            }
        });
        dialogUtils.showDialogPositiveNegative(getString(R.string.logout_title),
                getString(R.string.logout_from_profile),
                getString(R.string.yes), getString(R.string.no), true, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (drawer_layout.isDrawerOpen(Gravity.START)) {
                drawer_layout.closeDrawer(Gravity.START);
            } else {
                drawer_layout.openDrawer(Gravity.START);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    protected void bindHeaderViewHolder() {
        headerViewHolder.bindHolder();
    }

    class HeaderViewHolder {
        private Context context;
        @BindView(R.id.img_dn_avatar) ImageView img_dn_avatar;
        @BindView(R.id.tv_dn_full_name) TextView tv_dn_full_name;
        @BindView(R.id.tv_dn_iin) TextView tv_dn_iin;

        HeaderViewHolder(Context context, View view) {
            ButterKnife.bind(this, view);
            this.context = context;
        }

        void bindHolder() {
            GlideUtils.showAvatar(context, img_dn_avatar, preferences.getProfileImage(), R.drawable.avatar_white);
            tv_dn_full_name.setText(StringUtils.okTxt(preferences.getFullName(), tv_dn_full_name));
            tv_dn_iin.setText(getString(R.string.label_iin, StringUtils.okTxt(preferences.getUserIIN(), tv_dn_iin)));
        }

        @OnClick(R.id.ll_dn_container)
        void onProfileContainerClick() {
            if (getActivityId() == DN_MAIN_ACTIVITY) {
                drawer_layout.closeDrawer(ll_left_drawer);
                return;
            }
            drawer_layout.closeDrawer(ll_left_drawer);
            startActivity(new Intent(context, MainActivity.class));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // BaseMvpView implementation                                           ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_container, errorMessage);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onInvalidToken() {
        InvalidToken.wipeOut(this, dataLayer);
    }

    @Override
    public void appComingSoon() {
    }
}
