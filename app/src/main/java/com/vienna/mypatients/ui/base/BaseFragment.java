package com.vienna.mypatients.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.mvp.base.BaseMvpView;
import com.vienna.mypatients.utils.SnackbarUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {
    @BindView(R.id.fl_container) protected FrameLayout fl_container;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;

    FragmentNavigation mFragmentNavigation;
    protected DataLayer dataLayer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataLayer = DataLayer.getInstance(getContext());
    }

    protected abstract int getLayoutResourceId();

    protected abstract void onViewCreated();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, view);
        onViewCreated();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigation) context;
        }
    }

    public interface FragmentNavigation {
        public void pushFragment(Fragment fragment);
    }

    ///////////////////////////////////////////////////////////////////////////
    // BaseMvpView implementation                                           ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_container, errorMessage);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onInvalidToken() {
        InvalidToken.wipeOut(getContext(), dataLayer);
    }

    @Override
    public void appComingSoon() {
    }
}
