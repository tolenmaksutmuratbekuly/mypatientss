package com.vienna.mypatients.ui.base;

import android.content.Context;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.DataLayer;
import com.vienna.mypatients.ui.auth.AuthActivity;
import com.vienna.mypatients.views.dialogs.CommonDialog;

class InvalidToken {
    static void wipeOut(Context context, DataLayer dataLayer) {
        CommonDialog dialog = new CommonDialog(context);
        dialog.setCallbackSingleBtn(() -> {
            dataLayer.wipeOut();
            context.startActivity(AuthActivity.newLogoutIntent(context));
        });
        dialog.showDialogTitleDescSingleBtn(context.getString(R.string.invalid_token_title),
                context.getString(R.string.invalid_token),
                context.getString(R.string.ok), true, true);
    }
}
