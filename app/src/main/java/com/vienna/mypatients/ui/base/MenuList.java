package com.vienna.mypatients.ui.base;

public class MenuList {
    public String title;
    public int img;

    public MenuList(String title, int img) {
        this.title = title;
        this.img = img;
    }
}
