package com.vienna.mypatients.ui.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vienna.mypatients.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class MenuListAdapter extends BaseAdapter {
    private Context context;

    private List<MenuList> menuList = new ArrayList<>();

    MenuListAdapter(Context context, List<MenuList> menuList) {
        this.context = context;
        this.menuList = menuList;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_drawer_navigation, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.bindHolder(menuList.get(position), position);

        return convertView;
    }

    class ViewHolder {
        MenuList bindedObject;
        int bindedPosition;
        @BindView(R.id.tv_title) TextView tv_title;
        @BindView(R.id.img_icon) ImageView img_icon;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bindHolder(MenuList menuList, int position) {
            bindedObject = menuList;
            bindedPosition = position;
            if (menuList == null) {
                return;
            }
            tv_title.setText(menuList.title);
            img_icon.setImageResource(menuList.img);
        }
    }
}
