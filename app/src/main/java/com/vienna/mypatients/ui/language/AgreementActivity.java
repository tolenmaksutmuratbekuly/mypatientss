package com.vienna.mypatients.ui.language;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.mvp.language.AgreementPresenter;
import com.vienna.mypatients.mvp.language.AgreementView;
import com.vienna.mypatients.ui.auth.AuthActivity;
import com.vienna.mypatients.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class AgreementActivity extends BaseActivity implements AgreementView {
    @InjectPresenter
    AgreementPresenter mvpPresenter;
    @BindView(R.id.tv_agreement) TextView tv_agreement;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_agreement;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, AgreementActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    @OnClick(R.id.btn_reject)
    void onRejectClick() {
        finish();
    }

    @OnClick(R.id.btn_accept)
    void onAcceptClick() {
        mvpPresenter.onAcceptClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // AgreementView implementation                                          ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setAgreementContent(String text) {
        tv_agreement.setText(text);
    }

    @Override
    public void openAuthActivity() {
        startActivity(AuthActivity.getIntent(this));
        finish();
    }
}