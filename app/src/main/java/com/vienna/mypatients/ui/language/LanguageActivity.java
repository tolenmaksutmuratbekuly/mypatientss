package com.vienna.mypatients.ui.language;

import android.os.Bundle;
import android.widget.RadioButton;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.cache.preferences.Preferences;
import com.vienna.mypatients.mvp.language.LanguagePresenter;
import com.vienna.mypatients.mvp.language.LanguageView;
import com.vienna.mypatients.ui.auth.AuthActivity;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.Constants.KEY_LANG_KZ;
import static com.vienna.mypatients.utils.Constants.KEY_LANG_RU;
import static com.vienna.mypatients.utils.StringUtils.length;

public class LanguageActivity extends BaseActivity implements LanguageView {
    @BindView(R.id.rb_ru) RadioButton rb_ru;
    @BindView(R.id.rb_kz) RadioButton rb_kz;

    @InjectPresenter
    LanguagePresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_language;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Preferences preferences = dataLayer.getPreferences();
        if (preferences.isAuthenticated()) {
            startActivity(MainActivity.alreadyAuthorized(this));
            finish();
        } else {
            if (length(preferences.getLanguage()) > 0 && preferences.hasShownAgreement()) {
                startActivity(AuthActivity.getIntent(this));
                finish();
            } else if (length(preferences.getLanguage()) > 0) {
                startActivity(AgreementActivity.getIntent(this));
                finish();
            }
        }
        mvpPresenter.init(this, dataLayer);
    }

    @OnClick({R.id.rb_ru, R.id.rb_kz})
    void onLangChanged(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        if (checked) {
            switch (radioButton.getId()) {
                case R.id.rb_ru:
                    mvpPresenter.setSelectedLang(KEY_LANG_RU);
                    rb_kz.setChecked(false);
                    break;
                case R.id.rb_kz:
                    mvpPresenter.setSelectedLang(KEY_LANG_KZ);
                    rb_ru.setChecked(false);
                    break;
            }
        }
    }

    @OnClick(R.id.btn_continue)
    void onClickContinue() {
        mvpPresenter.onChangeLanguageClick();
    }

    ///////////////////////////////////////////////////////////////////////////
    // LanguageView implementation                                          ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setLang(boolean isRussian) {
        if (isRussian) rb_ru.setChecked(true);
        else rb_kz.setChecked(true);
    }

    @Override
    public void openAgreementActivity() {
        startActivity(AgreementActivity.getIntent(this));
        finish();
    }
}