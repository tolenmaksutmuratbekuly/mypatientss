package com.vienna.mypatients.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.BuildConfig;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.areas.Area;
import com.vienna.mypatients.data.models.profile.User;
import com.vienna.mypatients.mvp.main.MainPresenter;
import com.vienna.mypatients.mvp.main.MainView;
import com.vienna.mypatients.ui.areas.InfoFragment;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;
import com.vienna.mypatients.ui.base.view_pager.ViewPagerAdapter;
import com.vienna.mypatients.ui.main.profile.EditProfileImageDialog;
import com.vienna.mypatients.ui.patients.ListOfPatientActivty;
import com.vienna.mypatients.utils.ColorUtils;
import com.vienna.mypatients.utils.DateUtils;
import com.vienna.mypatients.utils.FileUtils;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.utils.ToastUtils;
import com.vienna.mypatients.utils.permission.PermissionRequestCodes;
import com.vienna.mypatients.utils.permission.PermissionUtils;
import com.vienna.mypatients.views.dialogs.CommonDialog;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.vienna.mypatients.utils.Constants.KEY_ROLE_SUPERVISOR;
import static com.vienna.mypatients.utils.Constants.PAGE_TYPE_MAIN_ACTIVITY;
import static com.vienna.mypatients.utils.Constants.SAMPLE_CROPPED_IMAGE_NAME;
import static com.vienna.mypatients.utils.Constants.TAB_COMPLAINS;
import static com.vienna.mypatients.utils.Constants.TAB_PATIENTS;
import static com.vienna.mypatients.utils.Constants.TAB_QUESTIONS;
import static com.vienna.mypatients.utils.StringUtils.EMPTY_STRING;
import static com.vienna.mypatients.utils.StringUtils.isStringOk;
import static com.vienna.mypatients.utils.permission.PermissionRequestCodes.STORAGE;
import static com.vienna.mypatients.utils.permission.PermissionUtils.PERMISSIONS_STORAGE;

public class MainActivity extends BaseDrawerNavActivity implements MainView {
    private static final int REQUEST_TAKE_PICTURE = 1;
    private static final int REQUEST_CHOOSE_PICTURE = 2;
    private Uri currentPhotoURI;

    @InjectPresenter
    MainPresenter mvpPresenter;

    @BindView(R.id.iv_avatar) CircleImageView ivAvatar;
    @BindView(R.id.tv_full_name) TextView tvFullName;
    @BindView(R.id.tv_rating) TextView tv_rating;
    @BindView(R.id.tv_reviews_count) TextView tv_reviews_count;
    @BindView(R.id.rating) RatingBar ratingBar;
    @BindView(R.id.tv_organization) TextView tvOrganization;
    @BindView(R.id.tv_iin) TextView tvIin;
    @BindView(R.id.tv_dob) TextView tv_dob;
    @BindView(R.id.tv_phone) TextView tvPhone;
    @BindView(R.id.tv_areas) TextView tvAreas;
    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.pager) ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private MenuItem menu_edit_profile;

    private CommonDialog dialogUtils;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getActivityId() {
        return DN_MAIN_ACTIVITY;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public static Intent alreadyAuthorized(Context context) {
        return new Intent(context, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mvpPresenter.init(this, dataLayer);
        dialogUtils = new CommonDialog(this);
        configureViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    @Override
    public void onBackPressed() {
        onLogoutClick();
    }

    @OnClick(R.id.iv_avatar)
    void onAvatarChangeClick() {
        mvpPresenter.onAvatarChangeClick();
    }

    @OnClick(R.id.btn_see)
    void onSeePatientsClick() {
        startActivity(ListOfPatientActivty.getIntent(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu_edit_profile = menu.findItem(R.id.menu_edit_profile);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_profile: {
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void configureViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        tabs.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }

    public boolean isPermissionGrantedToStorage() {
        return PermissionUtils.isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public boolean shouldShowRequestPermissionRationaleToStorage() {
        return ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));

        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.withMaxResultSize(256, 256);
        options.setCompressionQuality(100);
        options.withAspectRatio(1, 1);
        options.setFreeStyleCropEnabled(false);
        options.setToolbarColor(ColorUtils.getColor(this, R.color.actionBar));
        options.setStatusBarColor(ColorUtils.getColor(this, R.color.actionBar));
        options.setActiveWidgetColor(ColorUtils.getColor(this, R.color.actionBar));
        options.setToolbarTitle(getString(R.string.edit_photo));
        options.setHideBottomControls(false);
        options.setCircleDimmedLayer(true);
        uCrop.withOptions(options).start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TAKE_PICTURE: {
                if (resultCode == RESULT_OK) {
                    startCropActivity(currentPhotoURI);
                    currentPhotoURI = null;
                }
                break;
            }
            case REQUEST_CHOOSE_PICTURE: {
                if (data != null) {
                    Uri uri = data.getData();
                    startCropActivity(uri);
                }
                break;
            }
            case STORAGE: {
                if (isPermissionGrantedToStorage()) {
                    mvpPresenter.onActivityResultToStorage();
                }
                break;
            }
            case UCrop.REQUEST_CROP: {
                if (data != null) {
                    Uri resultUri = UCrop.getOutput(data);
                    if (resultUri != null) {
                        mvpPresenter.onCropActivityResultOk(resultUri);
                    }
                }
                break;
            }
            case UCrop.RESULT_ERROR: {
                if (data != null) {
                    Throwable cropError = UCrop.getError(data);
                    if (cropError != null
                            && cropError.getMessage() != null) {
                        ToastUtils.showToast(this, cropError.getMessage());
                    }
                }
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // MainView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setUser(User.Data response) {
        bindHeaderViewHolder();
        if (menu_edit_profile != null) {
            menu_edit_profile.setVisible(response.getRole().equals(KEY_ROLE_SUPERVISOR));
        }

        GlideUtils.showAvatar(this, ivAvatar, response.thumb, R.drawable.avatar_white);
        tvFullName.setText(StringUtils.okTxt(response.full_name, tvFullName));
        if (response.statistic != null) {
            // Complaints
            if (response.statistic.complaints != null) {
                if (response.statistic.complaints.advised != null &&
                        response.statistic.complaints.unadvised != null) {
                    int totalReviews = response.statistic.complaints.advised +
                            response.statistic.complaints.unadvised;
                    tv_reviews_count.setText(getString(R.string.prof_reviews, String.valueOf(totalReviews)));
                    float rating = 5 * totalReviews / 100;
                    tv_rating.setText(String.valueOf(rating) + " /");
                    ratingBar.setRating(rating);
                }
            }
        }

        // Areas
        if (response.areas != null && response.areas.size() > 0) {
            String areas = EMPTY_STRING;
            for (Area area : response.areas) {
                areas += area.name + ", ";
            }
            String boldWord = getString(R.string.prof_area);
            String wholeWord = boldWord + " " + StringUtils.okTxt(areas, tvAreas);
            tvAreas.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));
        }

        // Organization & role
        //response.role != null  use it for checking SuperDoctor
        String boldWord = EMPTY_STRING;
        if (response.organization != null && isStringOk(response.organization.name)) {
            boldWord = response.organization.name;
        }
        String wholeWord = boldWord + " / ";
        if (isStringOk(response.qualification)) {
            wholeWord = wholeWord + response.qualification;
        }
        tvOrganization.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));

        // IIN
        boldWord = getString(R.string.prof_iin);
        wholeWord = boldWord + " " + StringUtils.okTxt(response.iin, tvIin);
        tvIin.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));

        // Date of birthday
        boldWord = getString(R.string.prof_dob);
        wholeWord = boldWord + " " + StringUtils.okTxt(
                DateUtils.getBirthDateString(response.created_at), tv_dob);
        tv_dob.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));

        // Phone number
        boldWord = getString(R.string.prof_phone);
        wholeWord = boldWord + " " + StringUtils.okTxt(response.phone, tvPhone);
        tvPhone.setText(StringUtils.setBoldStyleToSpecificWord(boldWord, wholeWord));

        if (response.statistic != null) {
            adapter.addFrag(InfoFragment.newInstance(response.statistic, TAB_COMPLAINS, PAGE_TYPE_MAIN_ACTIVITY), getString(R.string.tab_complains));
            adapter.addFrag(InfoFragment.newInstance(response.statistic, TAB_PATIENTS, PAGE_TYPE_MAIN_ACTIVITY), getString(R.string.patients));
            adapter.addFrag(InfoFragment.newInstance(response.statistic, TAB_QUESTIONS, PAGE_TYPE_MAIN_ACTIVITY), getString(R.string.tab_questions));
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void openEditProfileImageDialog() {
        EditProfileImageDialog df = EditProfileImageDialog.newInstance();
        df.setCancelable(true);
        df.setCallback(new EditProfileImageDialog.Callback() {
            @Override
            public void onSelectProfileImageClick() {
                mvpPresenter.onSelectProfileImageClick(isPermissionGrantedToStorage(), shouldShowRequestPermissionRationaleToStorage());
            }

            @Override
            public void onTakePhotoClick() {
                mvpPresenter.onTakePhotoClick();
            }
        });
        df.show(getSupportFragmentManager(), EMPTY_STRING);
    }

    @Override
    public void selectProfileImageFromIntent() {
        Intent photoLibraryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoLibraryIntent.setType("image/*");
        this.startActivityForResult(photoLibraryIntent, REQUEST_CHOOSE_PICTURE);
    }

    @Override
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(this);
                // Save a file: path for use with ACTION_VIEW intents
//                currentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                currentPhotoURI = FileProvider.getUriForFile(this,
                        BuildConfig.FILE_PROVIDER,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoURI);
                this.startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE);
            }
        }
    }

    @Override
    public void showRequestPermissionRationaleToStorage() {
        dialogUtils.setCallbackPositiveNegative(new CommonDialog.CallbackPositiveNegative() {
            @Override
            public void onClickPositive() {
                mvpPresenter.onRationaleToStorageAllowed();
            }

            @Override
            public void onClickNegative() {

            }
        });
        dialogUtils.showDialogPositiveNegative(getString(R.string.rationale_storage), EMPTY_STRING,
                getString(R.string.allow), getString(R.string.reject), true, false);
    }

    @Override
    public void requestPermissionToStorage() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, PermissionRequestCodes.STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                mvpPresenter.onPermissionGrantedToStorage();
            } else {
                mvpPresenter.onPermissionNotGrantedToStorage(shouldShowRequestPermissionRationaleToStorage());
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showRequestPermissionDialogToStorage() {
        dialogUtils.setCallbackPositiveNegative(new CommonDialog.CallbackPositiveNegative() {
            @Override
            public void onClickPositive() {
                mvpPresenter.onDialogToStorageAllowed();
            }

            @Override
            public void onClickNegative() {

            }
        });
        dialogUtils.showDialogPositiveNegative(getString(R.string.rationale_storage) + "!", EMPTY_STRING,
                getString(R.string.allow), getString(R.string.reject), true, false);

    }

    @Override
    public void openSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        this.startActivityForResult(intent, STORAGE);
    }

    @Override
    public void profilePhotoSuccessUpdated() {
        ToastUtils.showToast(this, R.string.profile_image_updated_successfully);
    }
}