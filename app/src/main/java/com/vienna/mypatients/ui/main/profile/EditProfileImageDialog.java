package com.vienna.mypatients.ui.main.profile;

import android.app.Dialog;
import android.view.View;
import android.widget.TextView;

import com.vienna.mypatients.R;
import com.vienna.mypatients.ui.base.BaseBottomSheetDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileImageDialog extends BaseBottomSheetDialogFragment {

    private Callback callback;

    public static EditProfileImageDialog newInstance() {
        return new EditProfileImageDialog();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_edit_profile_image, null);
        dialog.setContentView(v);
        ButterKnife.bind(this, v);
        setBottomSheetCallback(v);
    }

    @OnClick({R.id.tv_select, R.id.tv_make_a_photo})
    void onMenuItemsClick(TextView textView) {
        if (textView.getId() == R.id.tv_select) {
            callback.onSelectProfileImageClick();
            dismiss();
        } else if (textView.getId() == R.id.tv_make_a_photo) {
            dismiss();
            callback.onTakePhotoClick();
        }
    }

    public interface Callback {
        void onSelectProfileImageClick();

        void onTakePhotoClick();
    }
}