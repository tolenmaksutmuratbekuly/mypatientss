package com.vienna.mypatients.ui.patients;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.mvp.patients.ListOfChildrenPresenter;
import com.vienna.mypatients.mvp.patients.ListOfChildrenView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;

import java.util.List;

import butterknife.BindView;

import static com.vienna.mypatients.utils.Constants.KEY_ID;

public class ListOfChildrenActivity extends BaseDrawerNavActivity implements ListOfChildrenView {
    @InjectPresenter
    ListOfChildrenPresenter mvpPresenter;
    private ListOfChildrenAdapter mAdapter;

    @BindView(R.id.recycler_view) RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        mvpPresenter.getListOfChildren(getIntent().getStringExtra(KEY_ID));

        configureRecyclerView();
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ListOfChildrenActivity.class);
    }

    private void configureRecyclerView() {
        mAdapter = new ListOfChildrenAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    public void setListOfChildrens(List<Children> childrens) {
        Toast.makeText(this, "Количество детей: " + childrens.size(), Toast.LENGTH_SHORT).show();
        mAdapter.addChildrens(childrens);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_list_of_children;
    }

    @Override
    protected int getActivityId() { return -1; }

}
