package com.vienna.mypatients.ui.patients;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.patients.Children;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;
import com.vienna.mypatients.views.fonts.TextViewOpenSansNeueRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOfChildrenAdapter extends RecyclerViewBaseAdapter{
    private static final int PATIENT_LAYOUT_ID = R.layout.adapter_list_of_patient;

    private Context context;
    private List<Children> data;

    ListOfChildrenAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void clearChildrens(){
        data.clear();
        notifyDataSetChanged();
    }

    void addChildrens(List<Children> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChildrenViewHolder(inflate(parent, PATIENT_LAYOUT_ID));
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        ((ChildrenViewHolder)holder).bind(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ChildrenViewHolder extends MainViewHolder {
        @BindView(R.id.tv_height) TextViewOpenSansNeueRegular tv_height;
        @BindView(R.id.tv_weight)TextViewOpenSansNeueRegular tv_weight;
        @BindView(R.id.tv_apgar_score) TextViewOpenSansNeueRegular tv_apgar_score;
        @BindView(R.id.tv_gender) TextViewOpenSansNeueRegular tv_gender;

        Children bindedChildren;
        int bindedPosition;

        ChildrenViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Children children, int position) {
            bindedChildren = children;
            bindedPosition = position;

            if (children == null) {
                return;
            }

            tv_height.setText(children.height);
            tv_weight.setText(children.weight);
            tv_apgar_score.setText(context.getString(R.string.area, children.apgar_score));
            tv_gender.setText(context.getString(R.string.list_of_patients_weeks, children.gender));
        }
    }
}
