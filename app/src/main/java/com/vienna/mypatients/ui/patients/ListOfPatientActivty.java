package com.vienna.mypatients.ui.patients;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.mvp.patients.ListOfPatientPresenter;
import com.vienna.mypatients.mvp.patients.ListOfPatientsView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;
import com.vienna.mypatients.utils.recycler_view.EndlessScrollListener;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.Constants.KEY_ID;

public class ListOfPatientActivty extends BaseDrawerNavActivity implements ListOfPatientsView {
    @InjectPresenter
    ListOfPatientPresenter mvpPresenter;
    private ListOfPatientAdapter mAdapter;

    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindView(R.id.btn_search) ImageButton btn_search;
    @BindView(R.id.search_filter) LinearLayout search_filter;
    @BindView(R.id.btn_search_apply) Button btn_search_apply;
    @BindView(R.id.edt_iin) AppCompatEditText edt_iin;
    @BindView(R.id.edt_full_name) AppCompatEditText edt_full_name;
    @BindView(R.id.sp_area) AppCompatSpinner sp_area;
    @BindView(R.id.chb_prenatal) AppCompatCheckBox chb_prenatal;
    @BindView(R.id.chb_postnatal) AppCompatCheckBox chb_postnatal;
    @BindView(R.id.chb_transfer) AppCompatCheckBox chb_transfer;
    @BindDrawable(R.drawable.divider_black) Drawable recycler_view_divider;

    public static Intent getIntent(Context context) {
        return new Intent(context, ListOfPatientActivty.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        actionBar.setDisplayShowTitleEnabled(true);
        configureRecyclerView();
    }

    private void configureRecyclerView() {
        mAdapter = new ListOfPatientAdapter(ListOfPatientActivty.this);
        mAdapter.setCallback(patient -> {
            Intent intent = PatientActivity.getIntent(ListOfPatientActivty.this);
            intent.putExtra(KEY_ID, patient.id);
            startActivity(intent);
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ListOfPatientActivty.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(recycler_view_divider);
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.addOnScrollListener(new EndlessScrollListener(recycler_view, 1) {
            @Override
            public void onRequestMoreItems() {
                if (!mAdapter.getLoading()) {
                    mvpPresenter.getListOfPatient(edt_full_name.getText().toString(),
                            edt_iin.getText().toString(),
                            chb_prenatal.isChecked(),
                            chb_postnatal.isChecked(),
                            chb_transfer.isChecked(),
                            true
                    );
                }
            }
        });
    }

    @Override
    public void setListOfPatients(List<Patient> patients) {
        mAdapter.addPatients(patients);
    }

    @Override
    public void showLoadingItemIndicator(boolean show) {
        mAdapter.setLoading(show);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_list_of_patient;
    }

    @Override
    protected int getActivityId() {
        return DN_LIST_OF_PATIENT_ACTIVITY;
    }

    @OnClick(R.id.btn_search)
    void setBtn_search() {
        if (recycler_view.getVisibility() == View.VISIBLE) {
            recycler_view.setVisibility(View.GONE);
            search_filter.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            search_filter.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_search_apply)
    void btn_search_apply() {
        search_filter.setVisibility(View.GONE);
        recycler_view.setVisibility(View.VISIBLE);
        mAdapter.clearPatients();
        mvpPresenter.getListOfPatient(edt_full_name.getText().toString(),
                edt_iin.getText().toString(),
                chb_prenatal.isChecked(),
                chb_postnatal.isChecked(),
                chb_transfer.isChecked(),
                false
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }


    @Override
    public void onBackPressed() {
        if (search_filter.getVisibility() == View.VISIBLE) {
            search_filter.setVisibility(View.GONE);
            recycler_view.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }
}
