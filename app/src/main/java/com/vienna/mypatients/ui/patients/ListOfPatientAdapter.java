package com.vienna.mypatients.ui.patients;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.views.fonts.TextViewOpenSansBold;
import com.vienna.mypatients.views.fonts.TextViewOpenSansNeueRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class ListOfPatientAdapter extends RecyclerViewBaseAdapter {
    private static final int PATIENT_LAYOUT_ID = R.layout.adapter_list_of_patient;

    private Context context;
    private List<Patient> data;
    private Callback callback;
    private boolean loading;

    ListOfPatientAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    boolean getLoading(){
        return loading;
    }

    void clearPatients(){
        data.clear();
        notifyDataSetChanged();
    }

    void addPatients(List<Patient> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case PATIENT_LAYOUT_ID:
                return new PatientViewHolder(inflate(parent, PATIENT_LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(parent, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else {
            return PATIENT_LAYOUT_ID;
        }
    }

    @Override
    public int getItemCount() {
        int count = data.size();
        if (loading) {
            count += 1;
        }
        return count;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case PATIENT_LAYOUT_ID:
                ((PatientViewHolder) holder).bind(data.get(position), position);
                break;
        }
    }

    class PatientViewHolder extends MainViewHolder {
        @BindView(R.id.img_view) ImageView imgView;
        @BindView(R.id.tv_full_name) TextViewOpenSansBold tvFullName;
        @BindView(R.id.tv_iin) TextViewOpenSansNeueRegular tvIin;
        @BindView(R.id.tv_area_name) TextViewOpenSansNeueRegular tvAreaName;
        @BindView(R.id.tv_martial_status) TextViewOpenSansNeueRegular tvMartialStatus;
        @BindView(R.id.tv_weeks) TextViewOpenSansNeueRegular tvWeeks;

        Patient bindedPatient;
        int bindedPosition;

        PatientViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Patient patient, int position) {
            bindedPatient = patient;
            bindedPosition = position;

            if (patient == null) {
                return;
            }

            GlideUtils.showAvatar(context, imgView, patient.image, R.drawable.avatar);
            tvFullName.setText(patient.full_name);
            tvIin.setText(patient.iin);
            tvAreaName.setText(patient.area.name);
            tvMartialStatus.setText(context.getString(R.string.period, patient.martial_status));
            tvWeeks.setText(context.getString(R.string.list_of_patients_weeks, patient.pregnancy!=null ? patient.pregnancy.postnatal_days : StringUtils.EMPTY_STRING));
        }

        @OnClick(R.id.rl_content)
        void onItemViewClick() {
            if (callback != null) {
                callback.onItemClick(bindedPatient);
            }
        }
    }

    interface Callback {
        void onItemClick(Patient patient);
    }
}
