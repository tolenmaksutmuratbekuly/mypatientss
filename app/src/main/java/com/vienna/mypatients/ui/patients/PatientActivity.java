package com.vienna.mypatients.ui.patients;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.patients.Patient;
import com.vienna.mypatients.mvp.patients.PatientPresenter;
import com.vienna.mypatients.mvp.patients.PatientView;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.ui.patients.finish_postnatal.FinishPostnatalActivity;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.StringUtils;

import butterknife.BindView;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.Constants.KEY_ID;

public class PatientActivity extends BaseActivity implements PatientView {
    @InjectPresenter
    PatientPresenter mvpPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_view) ImageView img_view;
    @BindView(R.id.tv_full_name) TextView tv_full_name;
    @BindView(R.id.tv_iin) TextView tv_iin;
    @BindView(R.id.tv_birth_date) TextView tv_birth_date;
    @BindView(R.id.tv_phone) TextView tv_phone;
    @BindView(R.id.tv_area_name) TextView tv_area_name;
    @BindView(R.id.tv_blood_group) TextView tv_blood_group;
    @BindView(R.id.tv_blood_rh) TextView tv_blood_rh;
    @BindView(R.id.tv_martial_status) TextView tv_martial_status;
    @BindView(R.id.tv_education) TextView tv_education;
    @BindView(R.id.tv_nationality) TextView tv_nationality;
    @BindView(R.id.tv_social_status) TextView tv_social_status;
    @BindView(R.id.tv_responsible_doctor) TextView tv_responsible_doctor;
    @BindView(R.id.btn_transfer_to_postnatal_over) RelativeLayout btn_transfer_to_postnatal_over;
    @BindView(R.id.btn_finish_to_prenatal_over) RelativeLayout btn_finish_to_prenatal_over;
    @BindView(R.id.btn_finish_to_postnatal_over) RelativeLayout btn_finish_to_postnatal_over;
    @BindView(R.id.btn_watch_childrens_data) RelativeLayout btn_watch_childrens_data;
    @BindView(R.id.btn_extraverted_calls) RelativeLayout btn_extraverted_calls;
    @BindView(R.id.btn_read_news) RelativeLayout btn_read_news;
    @BindView(R.id.btn_read_recomendations) RelativeLayout btn_read_recomendations;
    @BindView(R.id.btn_complaints) RelativeLayout btn_complaints;
    @BindView(R.id.btn_questions) RelativeLayout btn_questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mvpPresenter.getPatient(getIntent().getStringExtra(KEY_ID));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPatient(Patient patient) {
        GlideUtils.showAvatar(this, img_view, patient.image, R.drawable.avatar_white);
        tv_full_name.setText(patient.full_name);
        tv_iin.setText(patient.iin);
        tv_birth_date.setText(StringUtils.toString(patient.birth_date, "yyyy-MM-dd"));
        tv_phone.setText(patient.phone);
        tv_area_name.setText(patient.area != null ? patient.area.name : StringUtils.EMPTY_STRING);
        tv_blood_group.setText(patient.blood_group);
        tv_blood_rh.setText(patient.blood_rh);
        tv_martial_status.setText(patient.martial_status);
        tv_education.setText(patient.education);
        tv_nationality.setText(patient.nationality);
        tv_social_status.setText(patient.social_status);
        tv_responsible_doctor.setText(patient.responsible_doctor != null ? patient.responsible_doctor.full_name : StringUtils.EMPTY_STRING);
    }

    @Override
    public void openFinishPostnatalActivity(String patient_id, String pregnancies_id) {
        startActivity(FinishPostnatalActivity.getIntent(this, patient_id, pregnancies_id));
    }

    @OnClick(R.id.btn_transfer_to_postnatal_over)
    void setBtnTransfer_to_postnatal_over() {

    }

    @OnClick(R.id.btn_finish_to_prenatal_over)
    void setBtn_finish_to_prenatal_over() {

    }

    @OnClick(R.id.btn_finish_to_postnatal_over)
    void onFinishPostnatalClick() {
        mvpPresenter.onFinishPostnatalClick();
    }

    @OnClick(R.id.btn_watch_childrens_data)
    void setBtn_watch_childrens_data() {
        Intent intent = ListOfChildrenActivity.getIntent(PatientActivity.this);
        intent.putExtra(KEY_ID, getIntent().getStringExtra(KEY_ID));
        startActivity(intent);
    }

    @OnClick(R.id.btn_extraverted_calls)
    void setBtn_extraverted_calls() {

    }

    @OnClick(R.id.btn_read_news)
    void setBtn_read_news() {

    }

    @OnClick(R.id.btn_read_recomendations)
    void setBtn_read_recomendations() {

    }

    @OnClick(R.id.btn_complaints)
    void setBtn_complaints() {

    }

    @OnClick(R.id.btn_questions)
    void setBtn_questions() {

    }

    public static Intent getIntent(Context context) {
        return new Intent(context, PatientActivity.class);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_patient;
    }

}
