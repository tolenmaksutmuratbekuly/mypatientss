package com.vienna.mypatients.ui.patients.finish_postnatal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.mvp.patients.finish_postnatal.FinishPostnatalPresenter;
import com.vienna.mypatients.mvp.patients.finish_postnatal.FinishPostnatalView;
import com.vienna.mypatients.ui.base.BaseActivity;
import com.vienna.mypatients.utils.ToastUtils;
import com.vienna.mypatients.views.wheel_date_picker.WheelDatePicker;

import java.util.Calendar;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;

import static com.vienna.mypatients.utils.Constants.KEY_ID;
import static com.vienna.mypatients.utils.Constants.KEY_PREGNANCIES_ID;

public class FinishPostnatalActivity extends BaseActivity implements FinishPostnatalView {
    @InjectPresenter
    FinishPostnatalPresenter mvpPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.sp_reason) Spinner sp_reason;
    @BindView(R.id.wheel_date_picker) WheelDatePicker wheel_date_picker;

    @BindColor(R.color.white) int whiteColor;
    private String patient_id;
    private String pregnancies_id;

    public static Intent getIntent(Context context, String patient_id, String pregnancies_id) {
        Intent intent = new Intent(context, FinishPostnatalActivity.class);
        intent.putExtra(KEY_ID, patient_id);
        intent.putExtra(KEY_PREGNANCIES_ID, pregnancies_id);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_finish_postnatal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        if (getIntent() != null) {
            patient_id = getIntent().getStringExtra(KEY_ID);
            pregnancies_id = getIntent().getStringExtra(KEY_PREGNANCIES_ID);
        }

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        configureSpinner();
        configureDateWheel();
    }

    private void configureSpinner() {
        String[] reasonList = getResources().getStringArray(R.array.ishod_pregnancy_arr);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, reasonList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_reason.setAdapter(adapter);
        sp_reason.setSelection(0);
        sp_reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(whiteColor);
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER_HORIZONTAL);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void configureDateWheel() {
        wheel_date_picker.setCyclic(true);
        wheel_date_picker.setVisibleItemCount(5);
        wheel_date_picker.setSelectedItemTextColor(ContextCompat.getColor(this, R.color.actionBar));
        wheel_date_picker.setItemTextColor(ContextCompat.getColor(this, R.color.wheel_default));
//        wheel_date_picker.setCurved(true);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        wheel_date_picker.setYearFrame(currentYear - 5, currentYear);
        wheel_date_picker.setSelectedYear(currentYear - 5);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_cancel)
    void onCancelClick() {
        finish();
    }

    @OnClick(R.id.btn_finish)
    void onFinishClick() {
        mvpPresenter.finishPostnatal(patient_id, pregnancies_id, wheel_date_picker.getCurrentDate(),
                sp_reason.getSelectedItemPosition());
    }

    ///////////////////////////////////////////////////////////////////////////
    // FinishPostnatalView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void successResponse() {
        ToastUtils.showToast(this, getString(R.string.success_response));
    }
}
