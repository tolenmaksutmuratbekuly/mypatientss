package com.vienna.mypatients.ui.questions;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.mvp.questions.QuestionsPresenter;
import com.vienna.mypatients.mvp.questions.QuestionsView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;

public class QuestionsActivity extends BaseDrawerNavActivity implements QuestionsView{
    @InjectPresenter
    QuestionsPresenter mvpPresenter;
    private QuestionsAdapter mAdapter;

    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindDrawable(R.drawable.divider_black) Drawable recycler_view_divider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setDisplayShowTitleEnabled(true);
        mvpPresenter.init(this, dataLayer);
        mvpPresenter.getQuestions();
        toolbar.setTitle(getString(R.string.questions));
        configureRecyclerView();
    }

    private void configureRecyclerView() {
        mAdapter = new QuestionsAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    public void setQuestions(List<Question> questions) {
        mAdapter.addQuestions(questions);
    }

    @Override
    protected int getLayoutResourceId() { return R.layout.activity_recyclerview; }

    @Override
    protected int getActivityId() { return DN_QUESTIONS; }
}
