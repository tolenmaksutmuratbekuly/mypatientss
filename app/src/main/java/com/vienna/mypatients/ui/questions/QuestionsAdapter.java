package com.vienna.mypatients.ui.questions;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.questions.Question;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.views.fonts.TextViewOpenSansBold;
import com.vienna.mypatients.views.fonts.TextViewOpenSansNeueRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class QuestionsAdapter  extends RecyclerViewBaseAdapter {
    private static final int PATIENT_LAYOUT_ID = R.layout.adapter_questions;

    private Context context;
    private List<Question> data;

    QuestionsAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void clearQuestions(){
        data.clear();
        notifyDataSetChanged();
    }

    void addQuestions(List<Question> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QuestionViewHolder(inflate(parent, PATIENT_LAYOUT_ID));
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        ((QuestionViewHolder)holder).bind(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class QuestionViewHolder extends MainViewHolder {
        @BindView(R.id.img_view) CircleImageView img_view;
        @BindView(R.id.tv_author)TextViewOpenSansBold tv_author;
        @BindView(R.id.tv_created_at) TextViewOpenSansNeueRegular tv_created_at;
        @BindView(R.id.tv_iin) TextViewOpenSansNeueRegular tv_iin;
        @BindView(R.id.tv_area_name) TextViewOpenSansNeueRegular tv_area_name;
        @BindView(R.id.tv_martial_status) TextViewOpenSansNeueRegular tv_martial_status;
        @BindView(R.id.tv_weeks) TextViewOpenSansNeueRegular tv_weeks;
        @BindView(R.id.tv_medical_office) TextViewOpenSansNeueRegular tv_medical_office;
        @BindView(R.id.tv_status) TextViewOpenSansNeueRegular tv_status;
        @BindView(R.id.tv_comments_count) TextViewOpenSansNeueRegular tv_comments_count;

        Question bindedQuestion;
        int bindedPosition;

        QuestionViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Question question, int position) {
            bindedQuestion = question;
            bindedPosition = position;

            if (question == null) {
                return;
            }

            GlideUtils.showAvatar(context, img_view, question.patient.image, R.drawable.avatar);
            tv_author.setText(question.patient.full_name);
            tv_created_at.setText(StringUtils.toString(question.created_at, "dd.MM.yyyy"));
            tv_iin.setText(question.patient.iin);
            tv_area_name.setText(context.getString(R.string.area, question.patient.area));
            tv_martial_status.setText(context.getString(R.string.period, question.patient.status));
            tv_weeks.setText(context.getString(R.string.list_of_patients_weeks, StringUtils.EMPTY_STRING));
            tv_medical_office.setText(context.getString(R.string.medical_education, question.patient.organization));
            tv_status.setText(context.getString(R.string.question_status, question.status));
            tv_comments_count.setText(question.comments_count);

        }
    }
}
