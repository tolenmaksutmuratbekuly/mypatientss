package com.vienna.mypatients.ui.recommendations;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.mvp.recommendations.RecommendationCategoryPresenter;
import com.vienna.mypatients.mvp.recommendations.RecommendationCategoryView;
import com.vienna.mypatients.ui.base.BaseActivity;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;

public class RecommendationCategoryActivity extends BaseActivity implements RecommendationCategoryView{
    @InjectPresenter
    RecommendationCategoryPresenter mvpPresenter;
    private RecommendationCategoryAdapter mAdapter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindDrawable(R.drawable.divider_black) Drawable recycler_view_divider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mvpPresenter.init(this, dataLayer);
        mvpPresenter.getRecommendationCategory(getIntent().getStringExtra(RecommendationTypeActivity.TYPE));
        configureRecyclerView();
    }

    private void configureRecyclerView() {
        mAdapter = new RecommendationCategoryAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(recycler_view_divider);
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recommendation_category;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, RecommendationCategoryActivity.class);
    }

    @Override
    public void setRecommendationCategory(List<RecommendationCategory> category) {
        mAdapter.addCategory(category);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
