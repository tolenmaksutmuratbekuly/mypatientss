package com.vienna.mypatients.ui.recommendations;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.recommendations.RecommendationCategory;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;
import com.vienna.mypatients.utils.GlideUtils;
import com.vienna.mypatients.utils.StringUtils;
import com.vienna.mypatients.views.fonts.TextViewOpenSansBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecommendationCategoryAdapter extends RecyclerViewBaseAdapter {
    private static final int PATIENT_LAYOUT_ID = R.layout.adapter_recommendation_categories;

    private Context context;
    private List<RecommendationCategory> data;
    private Callback callback;

    RecommendationCategoryAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void addCategory(List<RecommendationCategory> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(inflate(parent, PATIENT_LAYOUT_ID));
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        ((CategoryViewHolder) holder).bind(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CategoryViewHolder extends MainViewHolder {
        @BindView(R.id.img_view) CircleImageView img_view;
        @BindView(R.id.tv_name) TextViewOpenSansBold tv_name;

        RecommendationCategory bindedCategory;
        int bindedPosition;

        CategoryViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(RecommendationCategory category, int position) {
            bindedCategory = category;
            bindedPosition = position;

            if (category == null) {
                return;
            }

            GlideUtils.showAvatar(context, img_view, category.thumb, R.drawable.avatar);
            tv_name.setText(category.name != null ? category.name.ru : StringUtils.EMPTY_STRING);
        }
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    interface Callback {
        void onItemClick(RecommendationCategory category);
    }
}
