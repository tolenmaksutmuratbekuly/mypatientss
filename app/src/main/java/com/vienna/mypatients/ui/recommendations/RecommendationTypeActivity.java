package com.vienna.mypatients.ui.recommendations;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.vienna.mypatients.R;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class RecommendationTypeActivity extends BaseDrawerNavActivity {

    public static final String TYPE = "natal";
    public static final String TYPE_PRENATAL = "prenatal";
    public static final String TYPE_POSTNATAL = "postnatal";

    @BindView(R.id.rl_prenatal) RelativeLayout rl_pregnats;
    @BindView(R.id.rl_postnatal) RelativeLayout rl_mothers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar.setDisplayShowTitleEnabled(true);
    }

    @OnClick(R.id.rl_prenatal)
    void setOnClickPrenatal(){
        Intent intent = RecommendationCategoryActivity.getIntent(this);
        intent.putExtra(TYPE, TYPE_PRENATAL);
        startActivity(intent);
    }

    @OnClick(R.id.rl_postnatal)
    void setOnClickPostnatal(){
        Intent intent = RecommendationCategoryActivity.getIntent(this);
        intent.putExtra(TYPE, TYPE_POSTNATAL);
        startActivity(intent);
    }

    @Override
    protected int getLayoutResourceId() { return R.layout.activity_recommendation_type; }

    @Override
    protected int getActivityId() { return DN_RECOMMENDATIONS_ACTIVITY; }
}
