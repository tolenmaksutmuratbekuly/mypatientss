package com.vienna.mypatients.ui.recommendations;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.mvp.recommendations.RecommendationPresenter;
import com.vienna.mypatients.mvp.recommendations.RecommendationView;
import com.vienna.mypatients.ui.base.BaseActivity;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;

public class RecommendationsActivity extends BaseActivity implements RecommendationView {
    @InjectPresenter
    RecommendationPresenter mvpPresenter;
    private RecommendationsAdapter mAdapter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindDrawable(R.drawable.divider_black) Drawable recycler_view_divider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mvpPresenter.init(this, dataLayer);
        mvpPresenter.getRecommendations(getIntent().getStringExtra(RecommendationTypeActivity.TYPE));
        configureRecyclerView();
    }

    private void configureRecyclerView() {
        mAdapter = new RecommendationsAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(recycler_view_divider);
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutResourceId() { return R.layout.activity_recommendation; }

    @Override
    public void setRecommendation(List<Recommendation> recommendations) {
        mAdapter.addRecommendation(recommendations);
    }
}
