package com.vienna.mypatients.ui.recommendations;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.recommendations.Recommendation;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class RecommendationsAdapter extends RecyclerViewBaseAdapter {
    private static final int PATIENT_LAYOUT_ID = R.layout.adapter_recommendations;

    private Context context;
    private List<Recommendation> data;
    private Callback callback;

    RecommendationsAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    void addRecommendation(List<Recommendation> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    void setCallback(Callback callback) {
        this.callback = callback;
    }

    interface Callback {
        void onItemClick(Recommendation category);
    }
}
