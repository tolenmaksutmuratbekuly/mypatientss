package com.vienna.mypatients.ui.support;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.mvp.support.SupportPresenter;
import com.vienna.mypatients.mvp.support.SupportView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class SupportActivity extends BaseDrawerNavActivity implements SupportView {
    @InjectPresenter
    SupportPresenter mvpPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.edt_message) AppCompatEditText edit_ext;
    @BindView(R.id.btn_send) Button btn_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        actionBar.setDisplayShowTitleEnabled(true);
    }

    @OnClick
    public void setBtn_send() {
        sendMessage(edit_ext.getText().toString());
    }

    @Override
    public void sendMessage(String message) {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_support;
    }

    @Override
    protected int getActivityId() {
        return DN_SUPPORT_ACTIVITY;
    }
}
