package com.vienna.mypatients.ui.versions;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.mvp.versions.VersionsPresenter;
import com.vienna.mypatients.mvp.versions.VersionsView;
import com.vienna.mypatients.ui.base.BaseDrawerNavActivity;
import com.vienna.mypatients.utils.DeviceUtils;
import com.vienna.mypatients.utils.recycler_view.EndlessScrollListener;

import java.util.List;

import butterknife.BindView;

import static com.vienna.mypatients.utils.Constants.KEY_LANG_RU;

public class VersionsActivity extends BaseDrawerNavActivity implements VersionsView {
    @InjectPresenter
    VersionsPresenter mvpPresenter;
    private VersionsAdapter mAdapter;

    @BindView(R.id.recycler_view) RecyclerView recycler_view;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recyclerview;
    }

    @Override
    protected int getActivityId() {
        return DN_VERSIONS_ACTIVITY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(this, dataLayer);
        actionBar.setDisplayShowTitleEnabled(true);
        toolbar.setTitle(getString(R.string.version_updates));
        fl_container.setBackgroundColor(ContextCompat.getColor(this, R.color.actionBar));
        configureRecyclerView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestroyView();
    }

    private void configureRecyclerView() {
        boolean isRussianLang = dataLayer.getPreferences().getLanguage().equals(KEY_LANG_RU);
        mAdapter = new VersionsAdapter(this, DeviceUtils.getAppVersion(), isRussianLang);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(mAdapter);
        recycler_view.addOnScrollListener(new EndlessScrollListener(recycler_view, 1) {
            @Override
            public void onRequestMoreItems() {
                if (!mAdapter.getLoading() && mAdapter.getItemCount() > 1) {
                    mvpPresenter.getVersionsList(true);
                }
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // VersionsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setVersions(List<Versions> versions) {
        mAdapter.addVersionsList(versions);
    }

    @Override
    public void showLoadingItemIndicator(boolean show) {
        mAdapter.setLoading(show);
    }
}