package com.vienna.mypatients.ui.versions;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vienna.mypatients.R;
import com.vienna.mypatients.data.models.extra.Versions;
import com.vienna.mypatients.ui.base.RecyclerViewBaseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class VersionsAdapter extends RecyclerViewBaseAdapter {
    private static final int HEADER_LAYOUT_ID = R.layout.adapter_versions_header;
    private static final int VERSION_UPDATES_LAYOUT_ID = R.layout.adapter_versions;

    private Context context;
    private List<Versions> data;
    private boolean loading;
    private String appVersion;
    private boolean isRussianLang;

    VersionsAdapter(Context context, String appVersion, boolean isRussianLang) {
        this.context = context;
        this.data = new ArrayList<>();
        this.appVersion = appVersion;
        this.isRussianLang = isRussianLang;
    }

    void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    boolean getLoading() {
        return loading;
    }

    void clearAreas() {
        data.clear();
        notifyDataSetChanged();
    }

    void addVersionsList(List<Versions> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_LAYOUT_ID:
                return new HeaderViewHolder(inflate(parent, HEADER_LAYOUT_ID));
            case VERSION_UPDATES_LAYOUT_ID:
                return new VersionsViewHolder(inflate(parent, VERSION_UPDATES_LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(parent, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_LAYOUT_ID;
        } else if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else {
            return VERSION_UPDATES_LAYOUT_ID;
        }
    }

    @Override
    public int getItemCount() {
        int count = data.size();
        if (loading) {
            count += 1;
        }
        return count + 1;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case HEADER_LAYOUT_ID:
                ((HeaderViewHolder) holder).bind();
                break;
            case VERSION_UPDATES_LAYOUT_ID:
                ((VersionsViewHolder) holder).bind(data.get(position), position);
                break;
        }
    }

    class HeaderViewHolder extends MainViewHolder {
        @BindView(R.id.tv_version) TextView tv_version;

        HeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind() {
            tv_version.setText(appVersion);
        }
    }

    class VersionsViewHolder extends MainViewHolder {
        @BindView(R.id.tv_version) TextView tv_version;
        @BindView(R.id.tv_description) TextView tv_description;

        Versions bindedObject;
        int bindedPosition;

        VersionsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Versions versions, int position) {
            bindedObject = versions;
            bindedPosition = position;

            if (versions == null) {
                return;
            }

            tv_version.setText(versions.getVersion());
            tv_description.setText(isRussianLang ? versions.getTextRU() : versions.getTextKZ());
        }
    }
}
