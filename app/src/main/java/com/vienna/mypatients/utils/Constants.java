package com.vienna.mypatients.utils;

public class Constants {
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String ACCESS_TOKEN = "Access-Token";
    public static final String CLIENT = "Client";
    public static final String UID = "Uid";
    public static final String LOCALE_LANG = "locale_lang";
    public static final String AGREEMENT = "agreement";
    public static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    public final static String KEY_ID = "id";
    public final static String KEY_PREGNANCIES_ID = "pregnancies_id";
    public final static String KEY_STATUS_COMPLETE = "complete";
    public final static String KEY_AREA = "area";
    public final static String KEY_PAGE_TYPE = "page_type";
    public final static String KEY_TYPE = "type";
    public static final String KEY_IMG = "img";
    public static final String KEY_FULL_NAME = "full_name";
    public static final String KEY_USER_IIN = "iin";
    public static final String KEY_ROLE_SUPERVISOR = "supervisor";

    public static final int TAB_COMPLAINS = 0;
    public static final int TAB_PATIENTS = 1;
    public static final int TAB_QUESTIONS = 2;
    public static final int PAGE_TYPE_MAIN_ACTIVITY = 0;
    public static final int PAGE_TYPE_AREA_DETAIL_ACTIVITY = 1;

    public static final int MAX_LENGTH_IIN = 12;
    public static final int MAX_LENGTH_CODE = 6;
    public static final int DEFAULT_MAX_LENGTH = 255;
    public static final String CONTENT = "content";

    public static final String KEY_LANG_RU = "ru";
    public static final String KEY_LANG_KZ = "kk";

    public static final int INITIAL_PAGE_NUMBER = 1;
    public static final int REQUEST_COUNT = 20;

}