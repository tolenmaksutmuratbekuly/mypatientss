package com.vienna.mypatients.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.vienna.mypatients.R;

public class GlideUtils {
    public static void showAvatar(Context context, ImageView view, String url, int placeholder) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(placeholder)
                .placeholder(placeholder)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }

    public static void showCampaignImgLarge(Context context, ImageView view, String url) {
        //1080x450
        int bgWidth = 1080;
        int bgHeight = 450;

        int attributes[] = DeviceUtils.getScreenWidthHeight(context);
        int displayWidth = attributes[0];
        int height = attributes[1];
//        view.getLayoutParams().width = displayWidth;
        view.getLayoutParams().height = height;

        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.color.colorPrimary)
                .crossFade()
                .override(displayWidth, height)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }
}