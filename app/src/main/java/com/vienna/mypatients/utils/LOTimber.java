package com.vienna.mypatients.utils;

import com.vienna.mypatients.BuildConfig;

import timber.log.Timber;

public class LOTimber {
    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Timber.d(message);
        }
    }
}
