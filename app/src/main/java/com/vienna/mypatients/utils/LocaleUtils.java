package com.vienna.mypatients.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.vienna.mypatients.data.cache.preferences.Preferences;

import java.util.Locale;

public class LocaleUtils {
    private Preferences preferences;
    private Context context;
    public LocaleUtils(Preferences preferences, Context context) {
        this.preferences = preferences;
        this.context = context;
    }

    public static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public Locale getLocale() {
        String savedLang = preferences.getLanguage();
        if (StringUtils.length(savedLang) > 0) {
            return new Locale(savedLang);
        } else {
            if (DeviceUtils.isAboveNougat()) {
                return context.getResources().getConfiguration().getLocales().get(0);
            } else {
                return context.getResources().getConfiguration().locale;
            }
        }
    }

    public void fixLocale(Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        Locale.setDefault(locale);
        if (DeviceUtils.isJellyBeanMR1OrAbove()) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
    }
}
