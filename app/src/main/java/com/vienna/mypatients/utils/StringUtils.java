package com.vienna.mypatients.utils;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;

import com.vienna.mypatients.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtils {
    public static int[] menuIcons = new int[]{
            R.drawable.ic_dn_profile,
            R.drawable.ic_dn_my_patients,
            R.drawable.ic_dn_my_areas,
            R.drawable.ic_dn_news,
            R.drawable.ic_dn_recommendation,
            R.drawable.ic_dn_questions,
            R.drawable.ic_dn_complains,
            R.drawable.ic_dn_calls,
            R.drawable.ic_dn_notifications,
            R.drawable.ic_dn_settings,
            R.drawable.ic_dn_support,
            R.drawable.ic_dn_version_updates,
            R.drawable.ic_dn_logout
    };

    public static String[] getDnMenuList(Context context) {
        return context.getResources().getStringArray(R.array.list_left_menu);
    }

    public static final String EMPTY_STRING = "";

    public static String trim(String s) {
        return s != null ? s.trim() : null;
    }

    public static int length(String s) {
        return s == null ? 0 : s.length();
    }

    public static String removeLastChar(String str) {
        if (length(str) > 0) {
            return str.substring(0, str.length() - 1);
        } else {
            return StringUtils.EMPTY_STRING;
        }
    }

    public static boolean isStringOk(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static String replaceNull(String text) {
        if (isStringOk(text)) {
            return text;
        } else {
            return EMPTY_STRING;
        }
    }

    public static String okTxt(String text, TextView textView) {
        if (isStringOk(text)) {
            textView.setVisibility(View.VISIBLE);
            return text;
        } else {
            textView.setVisibility(View.GONE);
            return EMPTY_STRING;
        }
    }

    public static String toString(Date date, String format) {
        DateFormat dateFormatter = new SimpleDateFormat(format);
        return dateFormatter.format(date);
    }

    public static SpannableStringBuilder setBoldStyleToSpecificWord(String boldWord, String wholeWord) {
        SpannableStringBuilder str = new SpannableStringBuilder(wholeWord);
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, boldWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return str;
    }
}