package com.vienna.mypatients.views.wheel_date_picker;

public interface IWheelDayPicker {
    int getSelectedDay();

    void setSelectedDay(int day);

    int getCurrentDay();

    void setYearAndMonth(int year, int month);

    int getYear();

    void setYear(int year);

    int getMonth();

    void setMonth(int month);
}