package com.vienna.mypatients.views.wheel_date_picker;

public interface IWheelMonthPicker {
    int getSelectedMonth();

    void setSelectedMonth(int month);

    int getCurrentMonth();
}
